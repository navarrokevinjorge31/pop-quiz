package app.review;

/**
 * Created by cp on 11/20/17.
 */

public class Config {

    //GOOGLE PROJECT ID
    public static final String GOOGLE_PROJ_ID = "276804030458";

    public static final String TYP_KEY = "notifmsg";
    public static final String MSG_KEY = "m";
    public static final String MSG_KEY1 = "pub_id";

    //REGISTER FB
    public static final String REGISTER_FB = "https://crowdpoint.org/pq/app/api/v1/user/registerFB";

    //REGISTER
    public static final String REGISTER = "https://crowdpoint.org/pq/app/api/v1/user/registerEmailPass";

    //LOGIN FB
    public static final String LOGIN = "https://crowdpoint.org/pq/app/api/v1/user/loginUser";

    //DEVICE ID
    public static final String DEVICEID = "https://crowdpoint.org/pq/app/api/v1/device/create";

    //GET QUESTION
    public static final String QUESTION = "https://crowdpoint.org/pq/app/api/v1/question/getSpecific";

    //SENDING ANSWER
    public static final String ANSWER = "https://crowdpoint.org/pq/app/api/v1/question/responseAnswer";

    //GETTING RANKING
    public static final String RANKING = "https://crowdpoint.org/pq/app/api/v1/question/getAllScoreRanking";

    //GETTING JOINED ACTIVITIES
    public static final String ACTIVITIES = "http://crowdpoint.org/pq/app/api/v1/seasonParticipated/getAllMySeasons";

    //GETTING PROFILE
    public static final String PROFILE = "https://crowdpoint.org/pq/app/api/v1/question/getMyScore";

    //UPDATE SCHOOL
    public static final String SCHOOL = "https://crowdpoint.org/pq/app/api/v1/user/updateSchoolProf";

    //UPLOAD PROFILE PICTURE
    public static final String PIMAGE = "https://crowdpoint.org/pq/app/api/v1/user/updateProfile";

    //VALIDATE ACCOUNT
    public static final String VALIDATE = "https://crowdpoint.org/pq/app/api/v1/account/retrievePaymentStat";

    //VALIDATE ACCOUNT
    public static final String PAYMENT = "https://crowdpoint.org/pq/app/api/v1/account/activate";

    //ANNOUNCEMENT
    public static final String ANNOUNCEMENT = "http://crowdpoint.org/pq/app/api/v1/annoucement/getById";

    //PROFILE SEASON
    public static final String PROFILE_SEASON = "http://crowdpoint.org/pq/app/api/v1/season/getSeasonProfile";

    //ALL SCORE
    public static final String MYSCORE_SEASON = "http://crowdpoint.org/pq/app/api/v1/question/getAllMyScore";

    //RECENT QUESTION
    public static final String RECENT_ANSWERED = "http://crowdpoint.org/pq/app/api/v1/question/getUserLastAnswered";
}
