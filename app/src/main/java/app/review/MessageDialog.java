package app.review;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by cp on 11/28/17.
 */

public class MessageDialog {

    Dialog mDialog;
    Context context;

    public void makeDialog(final Context context , String message){

        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.xml_times_up);
        Window window = mDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView t_message = mDialog.findViewById(R.id.txt_message);
        t_message.setText(message);
        Button b_ok = mDialog.findViewById(R.id.button_ok);
        b_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.hide();
            }
        });
        mDialog.show();

    }



}
