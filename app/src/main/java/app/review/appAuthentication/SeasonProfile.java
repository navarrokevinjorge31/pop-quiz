package app.review.appAuthentication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.review.Config;
import app.review.R;
import app.review.appTab.EndlessRecyclerOnScrollListener;
import app.review.appTab.MyDividerItemDecoration;
import app.review.appTab.TabActivity;
import app.review.appTab.TabActivity1;

import static app.review.appAuthentication.ConfirmActivity.TAG;

/**
 * Created by gics_admin on 2/1/2018.
 */

public class SeasonProfile extends Activity {

    String center_season_id , token , userid , cpayment , paid_status , season_banner;
    SessionManager session;
    TextView txt_name, txt_status , txt_start , txt_end , txt_season_reg_fee;
    ImageView seasonImg;
    Button btn_join , btn_back , btn_skip ,  btn_back_home;
    Dialog validate;
    private List<SeasonProfileItem> listPayment;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private SeasonProfileAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout ll_payments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_season_profile1);

        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");
        paid_status = intent.getStringExtra("paid_status");

        session = new SessionManager(this);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> usr = session.getUserID();
        userid = usr.get(SessionManager.KEY_USERID);

        txt_name = findViewById(R.id.txt_name);
        txt_status = findViewById(R.id.txt_status);
        txt_start = findViewById(R.id.txt_start);
        txt_end = findViewById(R.id.txt_end);
        seasonImg = findViewById(R.id.img_profile);
        btn_join = findViewById(R.id.btn_join);
        btn_back = findViewById(R.id.btn_back);
        btn_skip = findViewById(R.id.btn_skip);
        btn_back_home = findViewById(R.id.btn_back_home);
        recyclerView = findViewById(R.id.recyclerView);
        ll_payments = findViewById(R.id.ll_payments);
        txt_season_reg_fee= findViewById(R.id.txt_season_reg_fee);

        if(paid_status.equals("paid")){
         ll_payments.setVisibility(View.GONE);
         btn_join.setVisibility(View.VISIBLE);
         btn_skip.setVisibility(View.GONE);
        }

        btn_back_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SeasonProfile.this, TabActivity1.class);
                startActivity(intent);
                finishAffinity();
            }
        });

        //recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));

        listPayment = new ArrayList<>();
        mAdapter = new SeasonProfileAdapter(listPayment, SeasonProfile.this , new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d(TAG, "clicked position:" + i);
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                //newmakeStringReq();
            }
        });


        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayoutFragment);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                listPayment.clear();
                getSeasonProfile();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogValidate();
            }
        });

        btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(SeasonProfile.this, TabActivity.class);
                intent.putExtra("center_season_id",center_season_id);
                intent.putExtra("season_banner", season_banner);
                startActivity(intent);
                finishAffinity();

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getSeasonProfile();
    }

    public void getSeasonProfile(){


        String url = Config.PROFILE_SEASON +"/"+center_season_id+"/"+userid+"/"+token;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");

                            JSONObject c = obj.getJSONObject("data");
                            JSONObject d = c.getJSONObject("season_profile");

                            String season_prof_id = d.getString("season_prof_id");
                            String center_season_id = d.getString("center_season_id");
                            String season_prof_description = d.getString("season_prof_description");
                            String season_prof_status = d.getString("season_prof_status");
                            String season_prof_start = d.getString("season_prof_start");
                            String season_prof_end = d.getString("season_prof_end");
                            String season_prof_others = d.getString("season_prof_others");
                            String season_contact_number = d.getString("season_contact_number");
                            String season_prof_created = d.getString("season_prof_created");
                            String season_prof_updated = d.getString("season_prof_updated");
                            String season_profile_pic = d.getString("season_profile_pic");
                            String season_reg_fee  = d.getString("season_reg_fee");
                            season_banner = d.getString("season_banner");

                            txt_name.setText(season_prof_description);
                            txt_status.setText("Challenge Status: "+season_prof_status);
                            txt_start.setText("Challenge Start Date: "+season_prof_start);
                            txt_end.setText("Challenge End Date: "+season_prof_end);
                            txt_season_reg_fee.setText("Registration Fee: "+season_reg_fee+ " php");
                            Glide.with(getApplicationContext()).load("https://crowdpoint.org/pq_uploads/images/"+season_profile_pic)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(seasonImg);


                            JSONArray mode = d.getJSONArray("payment_option");
                            for (int i = 0; i < mode.length(); i++) {
                                JSONObject feedObj = mode.getJSONObject(i);

                                String pay_mode_id = feedObj.getString("pay_mode_id");
                                String center_season_id1 = feedObj.getString("center_season_id");
                                String pay_mode_code = feedObj.getString("pay_mode_code");
                                String pay_mode_description = feedObj.getString("pay_mode_description");
                                String pay_mode_created_by = feedObj.getString("pay_mode_created_by");
                                String pay_mode_created = feedObj.getString("pay_mode_created");
                                String pay_mode_updated = feedObj.getString("pay_mode_updated");


                                SeasonProfileItem m = new SeasonProfileItem(
                                        pay_mode_id,
                                        center_season_id1,
                                        pay_mode_code,
                                        pay_mode_description,
                                        pay_mode_created_by,
                                        pay_mode_created,
                                        pay_mode_updated);

                                listPayment.add(m);
                            }

                            mAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pub_id", center_season_id);
                params.put("user_unique_id", userid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        Log.d("strReq", String.valueOf(url));
        queue.add(postRequest);

    }


    public void validationAccount(){

        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.VALIDATE,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            String message = obj.getString("message");

                            if(success.equals("true")){
                                session.setLogin(true);
                                session.setPayment("paid");
                                Intent intent = new Intent(SeasonProfile.this, TabActivity1.class);
                                startActivity(intent);
                                finishAffinity();
                            }else{
                                dialogValidate();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());



                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);
        Log.d("validationurl", String.valueOf(postRequest));
    }

    public void dialogValidate(){

        validate = new Dialog(this);
        validate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        validate.setCancelable(false);
        validate.setContentView(R.layout.xml_validate_account);
        Window window = validate.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        TextView txt_message = validate.findViewById(R.id.txt_message);
        txt_message.setText("If you want to officially join the contest, you must activate your account by paying the registration fee. choose \"different mode of payments\" to join the challenge.\n" +
                "\n" +
                "or click \"continue as guest\" but you cannot received pop question notifications etc.,");


        Button btn_continue = validate.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                session.setLogin(true);
                Intent intent = new Intent(getApplicationContext(), TabActivity.class);
                intent.putExtra("center_season_id",center_season_id);
                intent.putExtra("season_banner", season_banner);
                startActivity(intent);
                validate.hide();
                finish();

            }
        });

        Button btn_payment = validate.findViewById(R.id.btn_payment);
        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validate.hide();

            }
        });

        if (!SeasonProfile.this.isFinishing()){
            validate.show();
        }
        //confirmAnswer.show();
    }

    @Override
    public void onBackPressed() {

    }
}
