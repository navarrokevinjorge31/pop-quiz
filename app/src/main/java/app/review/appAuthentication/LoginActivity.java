package app.review.appAuthentication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import app.review.Config;
import app.review.R;

/**
 * Created by cp on 11/20/17.
 */

public class LoginActivity extends Activity{

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 300;
    private PrefUtil prefUtil;
    LoginButton loginButton;
    String userid, token ,user , deviceId , phoneType;
    String deviceid = null;
    InstanceID instanceID;
    CallbackManager callbackManager;
    Context applicationContext;
    Thread t;
    SessionManager session;
    Button btn_login_activity , btn_register_activity , btn_start_challenge;

    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.INTERNET,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WAKE_LOCK};

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(LoginActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        printKeyHash();
        //checkPermissions();
        FacebookSdk.sdkInitialize(LoginActivity.this);
        AppEventsLogger.activateApp(LoginActivity.this);
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.xml_login);
        prefUtil = new PrefUtil(this);
        session = new SessionManager(getApplicationContext());
        instanceID = InstanceID.getInstance(getApplicationContext());

        t = new Thread() {

            @Override
            public void run() {
                try {

                    while (!isInterrupted()) {
                        Thread.sleep(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(deviceid==null){
                                    registerInBackground();
                                }else{
                                    t.isInterrupted();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        btn_start_challenge = findViewById(R.id.btn_start_challenge);
        btn_start_challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginAccount.class);
                startActivity(i);
                finish();
            }
        });


        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                deleteAccessToken();
                Profile profile = Profile.getCurrentProfile();
                String name = getName(profile);
                String accessToken = loginResult.getAccessToken().getToken();
                String id = loginResult.getAccessToken().getUserId();
                String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                prefUtil.saveAccessToken(accessToken);

                Log.d("Profile","Image:"+imageUrl+"\n"+"Name:"+name);

                Intent Confirm = new Intent(LoginActivity.this, ConfirmActivity.class);
                Confirm.putExtra("name", name);
                Confirm.putExtra("imageUrl", imageUrl);
                startActivity(Confirm);
                finish();

            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

    }

    private String getName(Profile profile) {
        StringBuilder stringBuffer = new StringBuilder();
        if (profile != null) {
            stringBuffer.append(profile.getName());
        }
        return stringBuffer.toString();
    }

    private void deleteAccessToken() {
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null){
                    //User logged out
                    prefUtil.clearToken();
                }
            }
        };
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {

            }
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";

                try {
                    if (instanceID == null) {
                        instanceID = InstanceID.getInstance(applicationContext);
                    }
                    deviceid = instanceID.getToken(Config.GOOGLE_PROJ_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    msg = "Registration ID :" + deviceid;
                    Log.d("deviceid",deviceid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }

                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("app.review", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("SHA: ", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

}
