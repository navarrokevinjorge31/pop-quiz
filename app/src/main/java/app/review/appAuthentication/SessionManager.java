package app.review.appAuthentication;

/**
 * Created by cp on 11/21/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "PopQuizLogin";

    public static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    public static final String KEY_IS_SET_SCHOOL = "isSetSchool";

    public static final String KEY_IS_ACTIVATED = "isActivated";
    public static final String KEY_USERID = "userid";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_PERMISSION = "permission";
    public static final String KEY_EMAIL_CONFIRM = "emailConfirm";
    public static final String KEY_PAYMENT = "payment";
    public static final String KEY_REG_TYPE = "regType";

    public static final String PROFILE_ID = "profile_id";
    public static final String FNAME = "fname";
    public static final String LNAME = "lname";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String CONTACT_NUMBER = "cnumber";
    public static final String BIRTHDAY = "birthday";
    public static final String GENDER = "gender";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void setActivated(boolean isActivated) {

        editor.putBoolean(KEY_IS_ACTIVATED, isActivated);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public boolean isActivated(){
        return  pref.getBoolean(KEY_IS_ACTIVATED,false);
    }

    public void setSchool(boolean isSetSchool) {

        editor.putBoolean(KEY_IS_SET_SCHOOL, isSetSchool);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login set school!");
    }

    public boolean isSetSchool(){
        return pref.getBoolean(KEY_IS_SET_SCHOOL, false);
    }

    public void setUserID(String Category) {
        editor.putString(KEY_USERID, Category);
        editor.commit();
    }

    public HashMap<String,String> getUserID(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USERID, pref.getString(KEY_USERID, null));
        return user;
    }

    public void setRegType(String regType) {
        editor.putString(KEY_REG_TYPE, regType);
        editor.commit();
    }

    public HashMap<String,String> getRegType(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_REG_TYPE, pref.getString(KEY_REG_TYPE, null));
        return user;
    }


    public void setToken(String Category) {
        editor.putString(KEY_TOKEN, Category);
        editor.commit();
    }

    public HashMap<String,String> getToken(){
        HashMap<String, String> recent = new HashMap<String, String>();
        recent.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));
        return recent;
    }

    public void setPermission(String Category) {
        editor.putString(KEY_PERMISSION, Category);
        editor.commit();
    }

    public HashMap<String,String> getPermission(){
        HashMap<String, String> recent = new HashMap<String, String>();
        recent.put(KEY_PERMISSION, pref.getString(KEY_PERMISSION, null));
        return recent;
    }

    public void setEmailConrfirm(String str) {
        editor.putString(KEY_EMAIL_CONFIRM, str);
        editor.commit();
    }

    public HashMap<String,String> getEmailConrfirm(){
        HashMap<String, String> recent = new HashMap<String, String>();
        recent.put(KEY_EMAIL_CONFIRM, pref.getString(KEY_EMAIL_CONFIRM, null));
        return recent;
    }

    public void setPayment(String str) {
        editor.putString(KEY_PAYMENT, str);
        editor.commit();
    }

    public HashMap<String,String> getPayment(){
        HashMap<String, String> recent = new HashMap<String, String>();
        recent.put(KEY_PAYMENT, pref.getString(KEY_PAYMENT, null));
        return recent;
    }

    //PROFILE ID
    public void setProfileId(String pid) {
        editor.putString(PROFILE_ID, pid);
        editor.commit();
    }

    public HashMap<String,String> getProfileId(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(PROFILE_ID, pref.getString(PROFILE_ID, null));
        return user;
    }

    //FIRSTNAME
    public void setFname(String fname) {
        editor.putString(FNAME, fname);
        editor.commit();
    }

    public HashMap<String,String> getFname(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(FNAME, pref.getString(FNAME, null));
        return user;
    }

    //LASTNAME
    public void setLname(String lname) {
        editor.putString(LNAME, lname);
        editor.commit();
    }

    public HashMap<String,String> getLname(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(LNAME, pref.getString(LNAME, null));
        return user;
    }

    //EMAIL ADDRESS
    public void setEmail(String str) {
        editor.putString(EMAIL, str);
        editor.commit();
    }

    public HashMap<String,String> getEmail(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(EMAIL, pref.getString(EMAIL, null));
        return user;
    }

    //ADDRESS
    public void setAddress(String str) {
        editor.putString(ADDRESS, str);
        editor.commit();
    }

    public HashMap<String,String> getAddress(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(ADDRESS, pref.getString(ADDRESS, null));
        return user;
    }

    //CONTACT NUMBER
    public void setCnumber(String str) {
        editor.putString(CONTACT_NUMBER, str);
        editor.commit();
    }

    public HashMap<String,String> getCnumber(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(CONTACT_NUMBER, pref.getString(CONTACT_NUMBER, null));
        return user;
    }

    //BIRTHDAY
    public void setBirthday(String str) {
        editor.putString(BIRTHDAY, str);
        editor.commit();
    }

    public HashMap<String,String> getBirthday(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(BIRTHDAY, pref.getString(BIRTHDAY, null));
        return user;
    }

    //GENDER
    public void setGender(String str) {
        editor.putString(GENDER, str);
        editor.commit();
    }

    public HashMap<String,String> getGender(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(GENDER, pref.getString(GENDER, null));
        return user;
    }

    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

    }


}