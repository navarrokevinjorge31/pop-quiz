package app.review.appAuthentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import app.review.Config;
import app.review.R;
import app.review.appTab.TabActivity;


/**
 * Created by cp on 11/20/17.
 */

public class ConfirmActivity extends Activity {

    public static final String TAG = ConfirmActivity.class.getSimpleName();
    String id , name , imageUrl , tokken;
    String userid, token ,user;
    TextView txt_name , txt_terms , txt_loadin_text;
    ImageView img_profile;
    String fullname , birthdate , gender , reg_type , profile_picture , email , password;
    FrameLayout loading;
    Button button;
    Thread t;
    String deviceid = null;
    private PrefUtil prefUtil;
    private SessionManager session;
    InstanceID instanceID;
    Context applicationContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_confirmactivity);

        t = new Thread() {

            @Override
            public void run() {
                try {


                    while (!isInterrupted()) {
                        Thread.sleep(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(deviceid==null){
                                    registerInBackground();
                                }else{
                                    t.isInterrupted();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        prefUtil = new PrefUtil(this);
        session = new SessionManager(getApplicationContext());
        instanceID = InstanceID.getInstance(getApplicationContext());

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        imageUrl = intent.getStringExtra("imageUrl");

        txt_loadin_text = findViewById(R.id.loadingText);
        txt_name =  findViewById(R.id.txt_name);
        txt_name.setText(name);

        img_profile = findViewById(R.id.img_profile);
        button =  findViewById(R.id.button);
        loading = findViewById(R.id.loading);
        txt_terms = findViewById(R.id.termsPolicy) ;
        txt_terms.setPaintFlags(txt_terms.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
//                Intent terms = new Intent(getApplicationContext(), TermsPolicy.class);
//                startActivity(terms);
            }
        });

        Glide.with(ConfirmActivity.this)
                .load(imageUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_profile);

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {

                        try {
                            id = object.getString("id");
                            fullname = object.getString("name");
                            txt_name.setText(fullname);
//                            birthdate = object.getString("birthday");
                            gender = object.getString("gender");
                            //Toast.makeText(ConfirmActivity.this, String.valueOf("datafacebook "+id+"\n"+fullname+"\n"+birthdate+"\n"+gender), Toast.LENGTH_SHORT).show();
                            Log.d("datafacebook",object.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Application code
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,gender,name,link");
        request.setParameters(parameters);
        request.executeAsync();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerRequest();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        disconnectFromFacebook();
//        Intent intent = new Intent(getApplicationContext(), LoginMain.class);
//        startActivity(intent);
        finishAffinity();
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }


    public void registerRequest(){
        loading.setVisibility(View.VISIBLE);

        String url = Config.REGISTER_FB;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            if(success.equals("false")){
                                loading.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "invalid account", Toast.LENGTH_SHORT).show();
                            }
                            JSONObject c = obj.getJSONObject("data");
                            String userId = c.getString("user_unique_id");
                            String token = c.getString("token");
                            String userLevel = c.getString("user_level");

                            session.setUserID(userId);
                            session.setToken(token);
                            session.setPermission(userLevel);
                            Log.d("session",userId +"\n"+ token +"\n"+ userLevel);

                            getDeviceID();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                        loading.setVisibility(View.GONE);
                    }

                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("birthdate", "2015-08-14");
                params.put("gender", gender);
                params.put("provider_uid", id);
                params.put("mobile_id", deviceid);
                params.put("profile_picture", "https://graph.facebook.com/" + id + "/picture?type=large");
                params.put("email", id);
                params.put("password", id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
        Log.d("strReq", String.valueOf(url));


    }

    public void loginRequest(){
        txt_loadin_text.setText("Logging in...");
        loading.setVisibility(View.VISIBLE);
        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.LOGIN,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            if(success.equals("false")){
                                loading.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "invalid account", Toast.LENGTH_SHORT).show();
                            }
                            JSONObject c = obj.getJSONObject("data");
                            String userId = c.getString("user_unique_id");
                            String token = c.getString("token");
                            String userLevel = c.getString("user_level");

                            session.setUserID(userId);
                            session.setToken(token);
                            session.setPermission(userLevel);
                            Log.d("session",userId +"\n"+ token +"\n"+ userLevel);

                            getDeviceID();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        loading.setVisibility(View.GONE);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("email",id);
                params.put("password", id);
                return params;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);

    }

    public void getDeviceID(){
        txt_loadin_text.setText("Logging in...");
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> uid = session.getUserID();
        HashMap<String, String> tkn = session.getToken();
        userid = uid.get(SessionManager.KEY_USERID);
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> usr = session.getPermission();
        user = usr.get(SessionManager.KEY_PERMISSION);

        //String url = "http://epaluto.com/crowdpoint2016/app/api/v1/device/create";

        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.DEVICEID,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(), " Login Successful!", Toast.LENGTH_SHORT).show();
                        session.setLogin(true);
                        finishAffinity();


                        t.interrupt();
                        Intent intent = new Intent(getApplicationContext(), TabActivity.class);
                        startActivity(intent);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                        loading.setVisibility(View.GONE);


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("device_unique_id", deviceid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);

    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {

            }
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";

                try {
                    if (instanceID == null) {
                        instanceID = InstanceID.getInstance(applicationContext);
                    }
                    deviceid = instanceID.getToken(Config.GOOGLE_PROJ_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    msg = "Registration ID :" + deviceid;
                    Log.d("deviceid",deviceid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }

                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }

}
