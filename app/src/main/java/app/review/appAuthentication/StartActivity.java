package app.review.appAuthentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;

import java.util.HashMap;

import app.review.R;
import app.review.appTab.TabActivity1;

/**
 * Created by cp on 11/23/17.
 */

public class StartActivity extends Activity {

    SessionManager session;
    String user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.xml_start_activity);
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        session = new SessionManager(getApplicationContext());
        HashMap<String, String> usr = session.getPermission();
        user = usr.get(SessionManager.KEY_PERMISSION);

        if (session.isLoggedIn()) {

            finishAffinity();
            Intent intent = new Intent(StartActivity.this, TabActivity1.class);
            startActivity(intent);
            finish();

        } else {
            Intent intent = new Intent(StartActivity.this, LoginAccount.class);
            startActivity(intent);
            finish();
        }
    }
}
