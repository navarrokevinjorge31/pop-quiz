package app.review.appAuthentication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import app.review.AppController;
import app.review.Config;
import app.review.ConnectivityReceiver;
import app.review.MessageDialog;
import app.review.R;
import app.review.appTab.TabActivity1;
import app.review.appUserManagement.RegisterA;

/**
 * Created by cp on 12/14/17.
 */

public class LoginAccount extends Activity implements ConnectivityReceiver.ConnectivityReceiverListener {
    String userid, token ,user , deviceid , cpayment;
    public static final String TAG = LoginAccount.class.getSimpleName();
    Button btn_login_activity , btn_register_activity , btn_visible;
    EditText et_email, et_password;
    FrameLayout loading;
    SessionManager session;
    MessageDialog m;
    InstanceID instanceID;
    Context applicationContext;
    Thread t;
    Dialog validate;
    String message , message1;
    CheckBox eulaChkBx;
    TextView termsPolicy;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_login_account);

        t = new Thread() {

            @Override
            public void run() {
                try {

                    while (!isInterrupted()) {
                        Thread.sleep(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(deviceid==null){
                                    registerInBackground();
                                }else{
                                    t.isInterrupted();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        instanceID = InstanceID.getInstance(getApplicationContext());
        m = new MessageDialog();
        session = new SessionManager(getApplicationContext());
        loading = findViewById(R.id.loading);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        eulaChkBx = findViewById( R.id.checkBox );
        btn_login_activity = findViewById(R.id.btn_login_activity);
        btn_login_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!eulaChkBx.isChecked()){
                    Toast.makeText(getApplicationContext(), "Please accept Terms of Service and Privacy Policy", Toast.LENGTH_SHORT).show();
                }else{

                    if(et_email.getText().toString().equals("")||et_email.getText().toString().equals(null)){
                        m.makeDialog(LoginAccount.this,"Email is required! \n Please fill up.");

                    }else if(et_password.getText().toString().equals("")||et_password.getText().toString().equals(null)){
                        m.makeDialog(LoginAccount.this,"Password is required! \n Please fill up.");

                    }else{
                        checkConnection();

                    }
                }


            }
        });

        btn_register_activity = findViewById(R.id.btn_register_activity);
        btn_register_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!eulaChkBx.isChecked()){
                    Toast.makeText(getApplicationContext(), "Please accept Terms of Service and Privacy Policy", Toast.LENGTH_SHORT).show();
                }else{

                    Intent i = new Intent(getApplicationContext(), RegisterA.class);
                    startActivity(i);
                }

            }
        });

        termsPolicy = findViewById(R.id.termsPolicy);
        termsPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), TermsPolicy.class);
                startActivity(i);
            }
        });

    }

    public void login(){

        loading.setVisibility(View.VISIBLE);
        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.LOGIN,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            message1 = obj.getString("message");

                            if(success.equals("false")){
                                loading.setVisibility(View.GONE);
                                }

                            JSONObject c = obj.getJSONObject("data");
                            String userId = c.getString("user_unique_id");
                            String token = c.getString("token");
                            String userLevel = c.getString("user_level");
                            String user_prof_id = c.getString("user_prof_id");
                            String isEmailConfirmed = c.getString("isEmailConfirmed");
                            String user_acc_status = c.getString("user_acc_status");

                            session.setUserID(userId);
                            session.setToken(token);
                            session.setPermission(userLevel);
                            session.setProfileId(user_prof_id);
                            session.setEmailConrfirm(isEmailConfirmed);
                            session.setPayment(user_acc_status);
                            Log.d("session",userId +"\n"+ token +"\n"+ userLevel +"\n"+ user_acc_status);

                            HashMap<String, String> uid = session.getEmailConrfirm();
                            String eConfirm = uid.get(SessionManager.KEY_EMAIL_CONFIRM);

                            if(eConfirm.equals("0")){
                                Toast.makeText(LoginAccount.this, "Please Confirm your email by clicking the link that we send to your email.", Toast.LENGTH_SHORT).show();
                                loading.setVisibility(View.GONE);
                            }else{
                                getDeviceID();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        loading.setVisibility(View.GONE);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("email",et_email.getText().toString());
                params.put("password", et_password.getText().toString());
                return params;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
    }

    public void getDeviceID(){
        loading.setVisibility(View.VISIBLE);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> uid = session.getUserID();
        HashMap<String, String> tkn = session.getToken();
        userid = uid.get(SessionManager.KEY_USERID);
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> usr = session.getPermission();
        user = usr.get(SessionManager.KEY_PERMISSION);

        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.DEVICEID,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
//                        session.setLogin(true);
//                        Intent intent = new Intent(getApplicationContext(), TabActivity.class);
//                        startActivity(intent);
//                        finishAffinity();

                        session.setLogin(true);
                        Intent intent = new Intent(LoginAccount.this, TabActivity1.class);
                        startActivity(intent);
                        finishAffinity();



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                        loading.setVisibility(View.GONE);


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("device_unique_id", deviceid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);

    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {

            }
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";

                try {
                    if (instanceID == null) {
                        instanceID = InstanceID.getInstance(applicationContext);
                    }
                    deviceid = instanceID.getToken(Config.GOOGLE_PROJ_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    msg = "Registration ID :" + deviceid;
                    Log.d("deviceid",deviceid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }

                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }








    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        int color;
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message1, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        if (!isConnected) {
            message1 = "Sorry! Not connected to internet";
            color = Color.RED;
            textView.setTextColor(color);

        } else {
            login();
        }
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


}
