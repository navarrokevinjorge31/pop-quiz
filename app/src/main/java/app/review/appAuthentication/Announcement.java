package app.review.appAuthentication;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.review.Config;
import app.review.R;

/**
 * Created by cp on 1/9/18.
 */

public class Announcement extends Activity {

    private String TAG = Announcement.class.getSimpleName();
    String announcement_id;
    TextView txt_announcement;
    ImageView ann_img;
    VideoView ann_vid;
    SessionManager session;
    String userid , token;
    MediaPlayer mediaplayer;
    MediaController mediaController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_announcement);

        Intent intent = getIntent();
        announcement_id = intent.getStringExtra("pub_id");

        mediaplayer = new MediaPlayer();
        mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaController = new MediaController(this);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> uid = session.getUserID();
        HashMap<String, String> tkn = session.getToken();
        userid = uid.get(SessionManager.KEY_USERID);
        token = tkn.get(SessionManager.KEY_TOKEN);

        txt_announcement = findViewById(R.id.txt_announcement);
        ann_img = findViewById(R.id.ann_img);
        ann_vid = findViewById(R.id.ann_vid);
        getAnnouncement();
    }

    public void getAnnouncement(){

        String url = Config.ANNOUNCEMENT+"/"+announcement_id+"/"+userid+"/"+token;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");

                            JSONObject c = obj.getJSONObject("data");
                            JSONObject d = c.getJSONObject("announcement");
                            String announcement = d.getString("ann_description");
                            String ann_img_ext = d.getString("ann_img_ext");
                            String ann_vid_ext = d.getString("ann_vid_ext");
                            String ann_img_name = d.getString("ann_img_name");
                            String ann_vid_name = d.getString("ann_vid_name");

                            if(ann_img_ext.equals("png")){
                                ann_img.setVisibility(View.VISIBLE);
                                Glide.with(getApplicationContext()).load("https://crowdpoint.org/pq_uploads/images/"+ann_img_name)
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(ann_img);
                            }else{
                                txt_announcement.setVisibility(View.VISIBLE);
                                ann_img.setVisibility(View.GONE);
                                txt_announcement.setText(announcement);
                            }

                            if(ann_vid_ext.equals("mp4")){

                                try {
                                    // Start the MediaController

                                    MediaController mediacontroller = new MediaController(
                                            Announcement.this);
                                    mediacontroller.setAnchorView(ann_vid);
                                    // Get the URL from String VideoURL
                                    Log.d("videoUrl","https://crowdpoint.org/pq_uploads/videos/"+ann_vid_name);
                                    Uri video = Uri.parse("https://crowdpoint.org/pq_uploads/videos/"+ann_vid_name);
                                    ann_vid.setMediaController(mediacontroller);
                                    ann_vid.setVideoURI(video);
                                    ann_vid.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        public void onCompletion(MediaPlayer mp) {
                                            ann_vid.start();
                                        }
                                    });

                                } catch (Exception e) {
                                    Log.e("Error", e.getMessage());
                                    e.printStackTrace();
                                }
                            }else{
                                txt_announcement.setVisibility(View.VISIBLE);
                                ann_vid.setVisibility(View.GONE);
                            }

                            txt_announcement.setText(announcement);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                    }
                }
        );
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);
        Log.d("strReq", String.valueOf(url));
    }
}
