package app.review.appAuthentication;

/**
 * Created by gics_admin on 2/5/2018.
 */

public class SeasonProfileItem {

    public String pay_mode_id;
    public String center_season_id;
    public String pay_mode_code;
    public String pay_mode_description;
    public String pay_mode_created_by;
    public String pay_mode_created;
    public String pay_mode_updated;



    public SeasonProfileItem(
            String pay_mode_id,
            String center_season_id,
            String pay_mode_code,
            String pay_mode_description,
            String pay_mode_created_by,
            String pay_mode_created,
            String pay_mode_updated){


        this.pay_mode_id = pay_mode_id;
        this.center_season_id = center_season_id;
        this.pay_mode_code = pay_mode_code;
        this.pay_mode_description = pay_mode_description;
        this.pay_mode_created_by = pay_mode_created_by;
        this.pay_mode_created = pay_mode_created;
        this.pay_mode_updated = pay_mode_updated;

    }
}
