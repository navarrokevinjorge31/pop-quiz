package app.review.appAuthentication;

/**
 * Created by cp on 11/21/17.
 */

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import app.review.Config;
import app.review.Question;
import app.review.R;


/**
 * Created by cp on 9/13/16.
 */
public class GCMNotificationIntentService extends IntentService {
    // Sets an ID for the notification, so it can be updated
    public static int notifyID = 0;
    NotificationCompat.Builder builder;

    Notification notification = null;

    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                sendNotification(extras.getString(Config.TYP_KEY),"Send error: " + extras.toString(),"");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                sendNotification(extras.getString(Config.TYP_KEY),"Deleted messages on server: "
                        + extras.toString(),"");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                    sendNotification(extras.getString(Config.TYP_KEY),extras.getString(Config.MSG_KEY),extras.getString(Config.MSG_KEY1));
            }
        }
        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String typActivity, String msg, String msg1) {
        Intent resultIntent;
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


//        mNotifyBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getBaseContext())
//                .setContentTitle("POP QUIZ")
//                .setContentText(msg1)
//                //.setSound(android.R.drawable.)
//                .setSmallIcon(R.drawable.ic_pop_quiz_logo_two);

//        notification = new NotificationCompat.Builder(this)
//                .setCategory(Notification.CATEGORY_PROMO)
//                .setContentTitle("POP QUIZ")
//                .setContentText(msg1)
//                .setSmallIcon(R.drawable.ic_pop_quiz_logo_two)
//                .setPriority(Notification.PRIORITY_HIGH)
//                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000}).build();

        int notifyID = 5;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_pop_quiz_logo_two)
                        .setContentTitle("POP QUIZ")
                        .setContentText(msg1)
                        .setColor(Color.parseColor("#009688"))
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_pop_quiz_logo_two))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setVibrate(new long[] { 100, 200, 300 })
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));



        if(typActivity.equals("task")) {
            resultIntent = new Intent(getBaseContext(), Question.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            resultIntent.putExtra("pub_id", msg1);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(getBaseContext(),
                    notifyID,
                    resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);

            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;
            if (notifyID < 9) {
                notifyID = notifyID + 1;
            } else {
                notifyID = 0;
            }
            mBuilder.setDefaults(defaults);
            mBuilder.setContentText("You have a new question");
            mBuilder.setAutoCancel(true);
            mNotificationManager.notify(notifyID + 1, mBuilder.build());

        } else if(typActivity.equals("announcement")) {
            resultIntent = new Intent(getBaseContext(), Announcement.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            resultIntent.putExtra("pub_id", msg1);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(getBaseContext(),
                    notifyID,
                    resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);

            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;
            if (notifyID < 9) {
                notifyID = notifyID + 1;
            } else {
                notifyID = 0;
            }
            mBuilder.setDefaults(defaults);
            mBuilder.setContentText("Announcement");
            mBuilder.setAutoCancel(true);
            mNotificationManager.notify(notifyID + 1, mBuilder.build());

        }
    }
}
