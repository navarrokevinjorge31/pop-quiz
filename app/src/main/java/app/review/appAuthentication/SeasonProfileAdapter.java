package app.review.appAuthentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.review.R;

/**
 * Created by gics_admin on 2/5/2018.
 */

public class SeasonProfileAdapter extends RecyclerView.Adapter<SeasonProfileAdapter.ViewHolder> {

    private Context context;
    private List<SeasonProfileItem> tabList;
    private List<SeasonProfileItem> dashboardListFiltered;

    public SeasonProfileAdapter(List<SeasonProfileItem> dbitems, Context context, AdapterView.OnItemClickListener onItemClickListener) {
        super();
        this.tabList = dbitems;
        this.dashboardListFiltered = dbitems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xml_mode_payment, parent, false);
        SeasonProfileAdapter.ViewHolder viewHolder = new SeasonProfileAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        SeasonProfileItem db = tabList.get(position);

        holder.txt_mode.setText(db.pay_mode_description);
        holder.txt_center_id.setText(db.center_season_id);
        holder.txt_pay_mode_code.setText(db.pay_mode_code);
        if(holder.txt_pay_mode_code.getText().toString().equals("1")){
            holder.txt_mode.setVisibility(View.GONE);
            holder.img_paymaya.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return tabList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txt_mode;
        public TextView txt_center_id;
        public TextView txt_pay_mode_code;
        public ImageView img_paymaya;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            txt_mode = itemView.findViewById(R.id.txt_mode);
            txt_center_id = itemView.findViewById(R.id.txt_center_id);
            txt_pay_mode_code = itemView.findViewById(R.id.txt_pay_mode_code);
            img_paymaya  = itemView.findViewById(R.id.img_paymaya);

        }

        @Override
        public void onClick(View v) {

            Log.e("txt_pay_mode_code",txt_pay_mode_code.getText().toString());

            if(txt_pay_mode_code.getText().toString().equals("1")){
                Intent intent = new Intent(context, Payment1.class);
                intent.putExtra("center_season_id",txt_center_id.getText().toString());
                context.startActivity(intent);
                ((Activity)context).finish();
            }else if(txt_pay_mode_code.getText().toString().equals("2")){

                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
            }else if(txt_pay_mode_code.getText().toString().equals("3")){
                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();

            }


        }


    }
}



