package app.review.appAuthentication;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.review.Config;
import app.review.R;
import app.review.appTab.TabActivity1;

/**
 * Created by gics_admin on 2/15/2018.
 */

public class Payment1 extends AppCompatActivity {

    public static final String TAG = Payment.class.getSimpleName();
    String userid , center_season_id;
    SessionManager session;
    WebView webView1;
    String redirect_url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_payment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Payment.");


        session = new SessionManager(this);
        HashMap<String, String> usr = session.getUserID();
        userid = usr.get(SessionManager.KEY_USERID);

        webView1 = findViewById(R.id.webview);
        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");
        paymentLink();



    }

    private void webView(){

        webView1.getSettings().setJavaScriptEnabled(true);
        webView1.setWebViewClient(new MyBrowser());
        webView1.loadUrl(redirect_url);
    }

    private class MyBrowser extends WebViewClient {
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }
    }

    // Metodo Navigating web page history
    @Override public void onBackPressed() {
        if(webView1.canGoBack()) {
            webView1.goBack();
        } else {
            super.onBackPressed();
        }
    }



    // Subclase WebViewClient() para Handling Page Navigation
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals(redirect_url)) { //Force to open the url in WEBVIEW
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }
    }


    public void paymentLink(){

        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.PAYMENT,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            JSONObject c = obj.getJSONObject("data");
                            String checkoutId = c.getString("checkoutId");
                            redirect_url = c.getString("redirect_url");
                            Log.e("paymaya link",redirect_url);
                            webView();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());



                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("center_season_id", center_season_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        Log.d("paymenturl", String.valueOf(postRequest));
        queue.add(postRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu., menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), TabActivity1.class);
                startActivity(intent);
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
