package app.review.appAuthentication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.WebView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.review.Config;
import app.review.R;

/**
 * Created by cp on 1/8/18.
 */

public class Payment extends Activity {

    public static final String TAG = Payment.class.getSimpleName();
    String userid , center_season_id;
    SessionManager session;
    private WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_payment);

        session = new SessionManager(this);
        HashMap<String, String> usr = session.getUserID();
        userid = usr.get(SessionManager.KEY_USERID);

        webView = findViewById(R.id.webview);
        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");
        paymentLink();
    }

    public void paymentLink(){

        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.PAYMENT,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            JSONObject c = obj.getJSONObject("data");
                            String checkoutId = c.getString("checkoutId");
                            String redirect_url = c.getString("redirect_url");

                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(redirect_url));
                            startActivity(browserIntent);

                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());



                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("center_season_id", center_season_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);

    }
}
