package app.review.appTab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.review.R;
import app.review.appAuthentication.SeasonProfile;
import app.review.appAuthentication.SessionManager;

/**
 * Created by gics_admin on 2/1/2018.
 */

public class TabAdapter extends RecyclerView.Adapter<TabAdapter.ViewHolder> {

    private Context context;
    private List<TabItem> tabList;
    private List<TabItem> dashboardListFiltered;
    SessionManager session;
    public TabAdapter(List<TabItem> dbitems, Context context, AdapterView.OnItemClickListener onItemClickListener){
        super();
        this.tabList = dbitems;
        this.dashboardListFiltered = dbitems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xml_tab_item, parent, false);
        TabAdapter.ViewHolder viewHolder = new TabAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        TabItem db =  tabList.get(position);

        holder.txt_imgurl.setText(db.season_profile);
        holder.txt_name.setText(db.season_name);
        holder.txt_status.setText("Participants: "+db.participant);
        holder.txt_center_id.setText(db.center_season_id);
        holder.txt_paid_status.setText(db.paid_status);

        Glide.with(context).load("https://crowdpoint.org/pq_uploads/images/"+holder.txt_imgurl.getText().toString())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.img_profile);
    }

    @Override
    public int getItemCount() {
        return tabList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView img_profile;
        public TextView txt_imgurl;
        public TextView txt_name;
        public TextView txt_status;
        public TextView txt_center_id;
        public TextView txt_paid_status;
        public ImageView img_info;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            img_profile = itemView.findViewById(R.id.img_profile);
            txt_imgurl = itemView.findViewById(R.id.txt_imgurl);
            txt_name =  itemView.findViewById(R.id.txt_name);
            txt_status = itemView.findViewById(R.id.txt_status);
            img_info = itemView.findViewById(R.id.img_info);
            txt_center_id = itemView.findViewById(R.id.txt_center_id);
            txt_paid_status = itemView.findViewById(R.id.txt_paid_status);

        }

        @Override
        public void onClick(View v) {

            session = new SessionManager(context);
            Log.e("txt_center_id",txt_center_id.getText().toString());

            if(txt_paid_status.getText().toString().equals("notpaid")){

                session.setPayment("notpaid");
                Intent intent = new Intent(context, SeasonProfile.class);
                intent.putExtra("paid_status","notpaid");
                intent.putExtra("center_season_id",txt_center_id.getText().toString());
                context.startActivity(intent);
                ((Activity)context).finish();

            }else{

                session.setPayment("paid");
                Intent intent = new Intent(context, SeasonProfile.class);
                intent.putExtra("paid_status","paid");
                intent.putExtra("center_season_id",txt_center_id.getText().toString());
                context.startActivity(intent);
                ((Activity)context).finish();

            }

        }

    }

}