package app.review.appTab;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import app.review.R;

/**
 * Created by gics_admin on 2/13/2018.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private Context context;
    private List<ProfileItem> pList;

    public ProfileAdapter(List<ProfileItem> dbitems, Context context, AdapterView.OnItemClickListener onItemClickListener) {
        super();
        this.pList = dbitems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xml_profile_item, parent, false);
        ProfileAdapter.ViewHolder viewHolder = new ProfileAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ProfileItem db = pList.get(position);

        holder.txt_season_name.setText(db.season_name);
        holder.txt_season_score.setText(db.positive_score);

    }

    @Override
    public int getItemCount() {
        return pList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txt_season_name;
        public TextView txt_season_score;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            txt_season_name = itemView.findViewById(R.id.txt_season_name);
            txt_season_score = itemView.findViewById(R.id.txt_season_score);
        }

        @Override
        public void onClick(View v) {
        }

    }

}