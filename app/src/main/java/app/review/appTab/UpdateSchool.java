package app.review.appTab;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.review.Config;
import app.review.MessageDialog;
import app.review.R;
import app.review.appAuthentication.LoginAccount;
import app.review.appAuthentication.Payment;
import app.review.appAuthentication.SessionManager;

/**
 * Created by cp on 11/29/17.
 */

public class UpdateSchool extends Activity{

    private String TAG = UpdateSchool.class.getSimpleName();
    EditText txt_school_name , txt_school_campus;
    Button button_confirm , button_cancel;
    FrameLayout loading;
    String userid , token;
    SessionManager session;
    Dialog validate;
    String message;
    MessageDialog m;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_update_school);

        session = new SessionManager(this);
        HashMap<String, String> uid = session.getUserID();
        userid = uid.get(SessionManager.KEY_USERID);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);

        txt_school_name = findViewById(R.id.txt_school_name);
        txt_school_campus = findViewById(R.id.txt_school_campus);
        button_confirm = findViewById(R.id.button_confirm);
        button_cancel = findViewById(R.id.button_cancel);
        loading = findViewById(R.id.loading);

        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSchool();
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void updateSchool(){

        loading.setVisibility(View.VISIBLE);

        String url = Config.SCHOOL;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            if(success.equals("true")){
                                session.setLogin(true);
                                Intent intent = new Intent(getApplicationContext(), LoginAccount.class);
                                startActivity(intent);
                                finishAffinity();
//                                Intent intent = new Intent(UpdateSchool.this, TabActivity.class);
//                                startActivity(intent);
//                                session.setSchool(true);
                            }else{

                                m = new MessageDialog();
                                m.makeDialog(UpdateSchool.this,"Something wrong \n Please Try again");
                                loading.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                        loading.setVisibility(View.GONE);
                    }

                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("token", token);
                params.put("user_school", txt_school_name.getText().toString() + " " + txt_school_campus.getText().toString());
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
        Log.d("strReq", String.valueOf(url));

    }


    @Override
    public void onBackPressed() {

    }


    public void validationAccount(){


        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.VALIDATE,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            message = obj.getString("message");

                            if(success.equals("false")){
                               dialogValidate();

                            }else{
                                session.setLogin(true);
                                Intent intent = new Intent(getApplicationContext(), TabActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());



                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);
    }


    public void dialogValidate(){

        validate = new Dialog(this);
        validate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        validate.setCancelable(false);
        validate.setContentView(R.layout.xml_validate_account);
        Window window = validate.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView txt_message = validate.findViewById(R.id.txt_message);
        txt_message.setText(message);

        TextView txt_message1 = validate.findViewById(R.id.txt_message1);
        txt_message1.setText("If you want to officially join the contest, you must activate your account by paying the registration fee. click \"payment using paymaya\" to proceed.\n" +
                "\n" +
                "        or click \"continue as guest\" but you cannot received pop question notifications, can't view current top quizzer, can't edit or update your account, etc.,\n" +
                "\n" +
                "        CONTINUE AS GUEST");

        Button btn_continue = validate.findViewById(R.id.btn_continue);
        btn_continue.setText("CONFIRM EMAIL AND LOGIN");
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //session.setLogin(true);
                Intent intent = new Intent(getApplicationContext(), LoginAccount.class);
                startActivity(intent);
                finishAffinity();

            }
        });

        Button btn_payment = validate.findViewById(R.id.btn_payment);
        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), Payment.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
                finishAffinity();

            }
        });

        if (!UpdateSchool.this.isFinishing()){
            validate.show();
        }
        //confirmAnswer.show();
    }
}
