package app.review.appTab;

/**
 * Created by gics_admin on 2/13/2018.
 */

public class ProfileItem {

    public String account_status;
    public String rev_center_id;
    public String season_name;
    public String season_type;
    public String season_status;
    public String correct_answer;
    public String incorrect_answer;
    public String positive_score;
    public String negative_score;


    public ProfileItem(
            String account_status,
            String rev_center_id,
            String season_name,
            String season_type,
            String season_status,
            String correct_answer,
            String incorrect_answer,
            String positive_score,
            String negative_score){


        this.account_status=account_status;
        this.rev_center_id=rev_center_id;
        this.season_name=season_name;
        this.season_type=season_type;
        this.season_status=season_status;
        this.correct_answer=correct_answer;
        this.incorrect_answer=incorrect_answer;
        this.positive_score=positive_score;
        this.negative_score=negative_score;

    }
}
