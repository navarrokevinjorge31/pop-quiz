package app.review.appTab;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.review.AppController;
import app.review.Config;
import app.review.R;
import app.review.appAuthentication.SeasonProfile;
import app.review.appAuthentication.SessionManager;
import app.review.appAuthentication.StartActivity;
import app.review.appUserManagement.PickImage1;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gics_admin on 2/13/2018.
 */

public class ProfileView1 extends AppCompatActivity {

    private String TAG = ProfileView1.class.getSimpleName();
    private String tag_string_req = "string_req";
    CircleImageView img_profile;
    TextView txt_name , txt_school , txt_score , txt_pid , txt_correct , txt_wrong , txt_recent;
    String token , userid , vimg_profile , profile_id , cpayment , center_season_id;
    SessionManager session;
    Button button_logout , button_join;
    Dialog logout;
    SwipeRefreshLayout swipeRefreshLayout;
    private List<ProfileItem> listPayment;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private ProfileAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_profile);

        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");

        session = new SessionManager(this);
        HashMap<String, String> uid = session.getUserID();
        userid = uid.get(SessionManager.KEY_USERID);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> pid = session.getProfileId();
        profile_id = pid.get(SessionManager.PROFILE_ID);
        HashMap<String, String> cpay = session.getPayment();
        cpayment = cpay.get(SessionManager.KEY_PAYMENT);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Profile");

        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));
        listPayment = new ArrayList<>();
        mAdapter = new ProfileAdapter(listPayment, ProfileView1.this , new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d(TAG, "clicked position:" + i);
            }
        });

        recyclerView.setAdapter(mAdapter);
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                //newmakeStringReq();
            }
        });

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayoutFragment);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                listPayment.clear();
                getSeasonScore();
                getProfile();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        img_profile = findViewById(R.id.img_profile);
        txt_pid = findViewById(R.id.txt_pid);
        txt_name =  findViewById(R.id.txt_name);
        txt_school =  findViewById(R.id.txt_school);
        txt_recent = findViewById(R.id.txt_recent);

        txt_recent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileView1.this, RecentAnswered.class);
                intent.putExtra("center_season_id",center_season_id);
                startActivity(intent);
                finish();
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileView1.this, PickImage1.class);
                intent.putExtra("vimg_profile", vimg_profile);
                startActivity(intent);
                finish();
            }
        });

        button_join = findViewById(R.id.button_join);

        if(cpayment.equals("paid")){
            button_join.setVisibility(View.GONE);
        }



        button_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileView1.this, SeasonProfile.class);
                intent.putExtra("paid_status","notpaid");
                intent.putExtra("center_season_id",center_season_id);
                startActivity(intent);
                finish();
            }
        });

        button_logout = findViewById(R.id.button_logout);
        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogout();
            }
        });

        getProfile();
        getSeasonScore();


    }


    public void getProfile(){

        final StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.PROFILE+"/"+center_season_id+"/"+userid+"/"+token, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString() );
                try {
                    JSONObject obj = new JSONObject(response);
                    String success = obj.getString("success");
                    String message = obj.getString("message");
                    JSONObject c = obj.getJSONObject("data");
                    JSONObject d = c.getJSONObject("myscore");

                    String vtxt_name = d.getString("user_fullname");
                    String vtxt_school = d.getString("user_school");
                    vimg_profile = d.getString("user_profile_picture");

                    txt_pid.setText("Profile id: "+profile_id);
                    txt_name.setText(vtxt_name.toUpperCase());
                    txt_school.setText(vtxt_school.toUpperCase());

                    Glide.with(ProfileView1.this)
                            .load("https://crowdpoint.org/pq_uploads/images/"+vimg_profile)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(img_profile);
//                    for (int i = 0; i < d.length(); i++) {
//                        JSONObject feedObj = d.getJSONObject(i);
//
//
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }

        });
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        Log.d("strReq", String.valueOf(strReq));
    }


    public void getSeasonScore(){


        String url = Config.MYSCORE_SEASON +"/"+userid+"/1";
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");

                            JSONObject c = obj.getJSONObject("data");
                            JSONArray d = c.getJSONArray("myscore");

                            for (int i = 0; i < d.length(); i++) {
                                JSONObject feedObj = d.getJSONObject(i);

                                String account_status = feedObj.getString("account_status");
                                String rev_center_id = feedObj.getString("rev_center_id");
                                String season_name = feedObj.getString("season_name");
                                String season_type = feedObj.getString("season_type");
                                String season_status = feedObj.getString("season_status");
                                String correct_answer = feedObj.getString("correct_answer");
                                String incorrect_answer = feedObj.getString("incorrect_answer");
                                String positive_score = feedObj.getString("positive_score");
                                String negative_score = feedObj.getString("negative_score");

                                ProfileItem m = new ProfileItem(
                                        account_status,
                                        rev_center_id,
                                        season_name,
                                        season_type,
                                        season_status,
                                        correct_answer,
                                        incorrect_answer,
                                        positive_score,
                                        negative_score);

                                listPayment.add(m);
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        Log.d("strReq", String.valueOf(url));
        queue.add(postRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu., menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    public void userLogout(){

        logout = new Dialog(this);
        logout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logout.setContentView(R.layout.xml_logout);
        Window window = logout.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button yes = (Button) logout.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logoutUser();
                logout.dismiss();
                finishAffinity();
                Intent d = new Intent(ProfileView1.this, StartActivity.class);
                startActivity(d);
            }
        });
        Button no = (Button) logout.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout.dismiss();
            }
        });
        logout.show();
    }
}
