package app.review.appTab;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.review.AppController;
import app.review.Config;
import app.review.R;
import app.review.appAuthentication.SeasonProfile;
import app.review.appAuthentication.SessionManager;
import app.review.appAuthentication.StartActivity;
import app.review.appUserManagement.PickImage1;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by cp on 11/24/17.
 */

public class ProfileView extends AppCompatActivity {

    private String TAG = ProfileView.class.getSimpleName();
    private String tag_string_req = "string_req";
    CircleImageView img_profile;
    TextView txt_name , txt_school , txt_score , txt_pid , txt_correct , txt_wrong , txt_recent;
    String token , userid , vimg_profile , profile_id , cpayment , center_season_id;
    SessionManager session;
    Button button_logout , button_join;
    Dialog logout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_profile1);

        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");
        session = new SessionManager(this);
        HashMap<String, String> uid = session.getUserID();
        userid = uid.get(SessionManager.KEY_USERID);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> pid = session.getProfileId();
        profile_id = pid.get(SessionManager.PROFILE_ID);
        HashMap<String, String> cpay = session.getPayment();
        cpayment = cpay.get(SessionManager.KEY_PAYMENT);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Profile");

        img_profile = findViewById(R.id.img_profile);
        txt_name =  findViewById(R.id.txt_name);
        txt_school =  findViewById(R.id.txt_school);
        txt_score =  findViewById(R.id.txt_points);
        txt_pid = findViewById(R.id.txt_pid);
        txt_correct = findViewById(R.id.txt_correct);
        txt_wrong = findViewById(R.id.txt_wrong);
        txt_recent = findViewById(R.id.txt_recent);

        txt_recent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileView.this, RecentAnswered.class);
                intent.putExtra("center_season_id",center_season_id);
                startActivity(intent);
                finish();
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileView.this, PickImage1.class);
                intent.putExtra("vimg_profile", vimg_profile);
                startActivity(intent);
                finish();
            }
        });

        button_join = findViewById(R.id.button_join);

        if(cpayment.equals("paid")){
          button_join.setVisibility(View.GONE);
        }

        button_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileView.this, SeasonProfile.class);
                intent.putExtra("paid_status","notpaid");
                intent.putExtra("center_season_id",center_season_id);
                startActivity(intent);
                finish();
            }
        });

        button_logout = findViewById(R.id.button_logout);
        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogout();
            }
        });
//
        getProfile();
    }

    public void getProfile(){

        final StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.PROFILE+"/"+center_season_id+"/"+userid+"/"+token, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString() );
                try {
                    JSONObject obj = new JSONObject(response);
                    String success = obj.getString("success");
                    String message = obj.getString("message");
                    JSONObject c = obj.getJSONObject("data");
                    JSONObject d = c.getJSONObject("myscore");

                    String vtxt_name = d.getString("user_fullname");
                    String vtxt_school = d.getString("user_school");
                    String vtxt_score = d.getString("positive_score");
                    String vtxt_correct = d.getString("correct_answer");
                    String vtxt_wrong = d.getString("incorrect_answer");
                    vimg_profile = d.getString("user_profile_picture");

                    txt_pid.setText("Profile id: "+profile_id);
                    txt_name.setText(vtxt_name.toUpperCase());
                    txt_school.setText(vtxt_school.toUpperCase());
                    txt_score.setText(vtxt_score);
                    txt_correct.setText(vtxt_correct);
                    txt_wrong.setText(vtxt_wrong);
                    txt_recent.setText("My Recent Answered Question");

                    Glide.with(ProfileView.this)
                            .load("https://crowdpoint.org/pq_uploads/images/"+vimg_profile)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(img_profile);
//                    for (int i = 0; i < d.length(); i++) {
//                        JSONObject feedObj = d.getJSONObject(i);
//
//
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }

        });
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        Log.d("strReq", String.valueOf(strReq));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu., menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    public void userLogout(){

        logout = new Dialog(this);
        logout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logout.setContentView(R.layout.xml_logout);
        Window window = logout.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button yes = (Button) logout.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logoutUser();
                logout.dismiss();
                disconnectFromFacebook();
                finishAffinity();
                Intent d = new Intent(ProfileView.this, StartActivity.class);
                startActivity(d);
            }
        });
        Button no = (Button) logout.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout.dismiss();
            }
        });
        logout.show();
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }
}