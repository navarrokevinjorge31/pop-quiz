package app.review.appTab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import app.review.R;

public class TabActivity extends Activity {
    ImageView background;
    String center_season_id , season_banner;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {

                case R.id.navigation_home:
                    Intent home = new Intent(TabActivity.this, TabActivity1.class);
                    startActivity(home);
                    finishAffinity();
                    return true;
                case R.id.navigation_dashboard:

                    Intent intent = new Intent(TabActivity.this, DashBoard.class);
                    intent.putExtra("center_season_id",center_season_id);
                    startActivity(intent);

                    return true;
                case R.id.navigation_notifications:

                    Intent intent1 = new Intent(TabActivity.this, ProfileView1.class);
                    intent1.putExtra("center_season_id",center_season_id);
                    startActivity(intent1);

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        
        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");
        season_banner = intent.getStringExtra("season_banner");
        background = findViewById(R.id.background);

        Log.e("season_banner","https://crowdpoint.org/pq_uploads/images/"+season_banner);
        Glide.with(getApplicationContext()).load("https://crowdpoint.org/pq_uploads/images/"+season_banner)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(background);

    }


    @Override
    public void onBackPressed() {

    }
}
