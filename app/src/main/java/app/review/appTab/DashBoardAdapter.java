package app.review.appTab;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import app.review.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by cp on 11/28/17.
 */

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.ViewHolder> implements Filterable {

    private Context context;
    private List<DashBoardItem> dashboardList;
    private List<DashBoardItem> dashboardListFiltered;

    public DashBoardAdapter(List<DashBoardItem> dbitems, Context context){
        super();
        this.dashboardList = dbitems;
        this.dashboardListFiltered = dbitems;
        this.context = context;
    }

    @Override
    public DashBoardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xml_dashboard_item, parent, false);
        DashBoardAdapter.ViewHolder viewHolder = new DashBoardAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DashBoardAdapter.ViewHolder holder, int position) {

        DashBoardItem db =  dashboardList.get(position);

        holder.txt_imgurl.setText(db.user_profile_picture);
        holder.txt_name.setText(db.user_fullname);
        holder.txt_school.setText(db.user_school);
        holder.txt_score.setText(db.positive_score);

        if(!db.positive_score.equals("0")){
            holder.txt_rank.setText("Rank: "+String.valueOf(Integer.parseInt(db.user_rank) + 1));
        }else{
            holder.txt_rank.setText("no rank");
        }


        Glide.with(context).load("https://crowdpoint.org/pq_uploads/images/"+holder.txt_imgurl.getText().toString())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.img_profile);

    }

    @Override
    public int getItemCount() {
        return dashboardList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    dashboardList = dashboardListFiltered;
                } else {
                    List<DashBoardItem> filteredList = new ArrayList<>();
                    for (DashBoardItem row : dashboardListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.user_fullname.toLowerCase().contains(charString.toLowerCase())|| row.user_school.toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    dashboardList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dashboardList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dashboardList = (ArrayList<DashBoardItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public CircleImageView img_profile;
        public TextView txt_imgurl;
        public TextView txt_name;
        public TextView txt_school;
        public TextView txt_score;
        public TextView txt_rank;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            img_profile = itemView.findViewById(R.id.img_profile);
            txt_imgurl = itemView.findViewById(R.id.txt_imgurl);
            txt_name =  itemView.findViewById(R.id.txt_name);
            txt_school = itemView.findViewById(R.id.txt_school);
            txt_score = itemView.findViewById(R.id.txt_score);
            txt_rank = itemView.findViewById(R.id.txt_rank);

        }

        @Override
        public void onClick(View v) {

        }


    }

    public interface DashboardAdapterListener {
        void onContactSelected(DashBoardItem contact);
    }

}

