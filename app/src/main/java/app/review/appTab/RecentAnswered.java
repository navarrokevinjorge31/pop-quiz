package app.review.appTab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.review.Config;
import app.review.R;
import app.review.appAuthentication.SessionManager;

/**
 * Created by gics_admin on 2/8/2018.
 */

public class RecentAnswered extends AppCompatActivity {

    public static final String TAG = RecentAnswered.class.getSimpleName();
    String center_season_id , userid , token;
    SessionManager session;
    TextView txt_dLimit , txt_rQuestion , txt_uAnswer , txt_rAnswer , txt_aStatus;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_recent_answered);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Recent Answered");

        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");

        session = new SessionManager(RecentAnswered.this);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> usr = session.getUserID();
        userid = usr.get(SessionManager.KEY_USERID);

        txt_dLimit    = findViewById(R.id.txt_dLimit);
        txt_rQuestion = findViewById(R.id.txt_rQuestion);
        txt_uAnswer   = findViewById(R.id.txt_uAnswer);
        txt_rAnswer   = findViewById(R.id.txt_rAnswer);
        txt_aStatus   = findViewById(R.id.txt_aStatus);

        getRecentAnswered();
    }

    public void getRecentAnswered(){

        String url = Config.RECENT_ANSWERED +"/"+userid+"/"+ center_season_id+"/"+token;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");

                            JSONObject c = obj.getJSONObject("data");
                            JSONObject d = c.getJSONObject("pub_details");

                            String datetimelimit = d.getString("datetimelimit");
                            String time_status = d.getString("time_status");
                            String pub_date_limit = d.getString("pub_date_limit");
                            String pub_time_limit = d.getString("pub_time_limit");
                            String answer_id = d.getString("answer_id");
                            String pub_id = d.getString("pub_id");
                            String user_unique_id = d.getString("user_unique_id");
                            String user_answer = d.getString("user_answer");
                            String answer_status = d.getString("answer_status");
                            String answer_remarks = d.getString("answer_remarks");
                            String answer_created = d.getString("answer_created");
                            String answer_updated = d.getString("answer_updated");
                            String ques_correct = d.getString("ques_correct");
                            String ques_desc = d.getString("ques_desc");
                            String standard_time = d.getString("standard_time");

                            txt_dLimit.setText("QUESTION POP TIME: "+datetimelimit);
                            txt_rQuestion.setText("QUESTION:"+ques_desc);
                            txt_uAnswer.setText("YOUR ANSWER: "+user_answer);
                            txt_rAnswer.setText("CORRECT ANSWER: "+ques_correct);
                            txt_aStatus.setText("STATUS: "+answer_status);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pub_id", center_season_id);
                params.put("user_unique_id", userid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        Log.d("strReq", String.valueOf(url));
        queue.add(postRequest);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu., menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
