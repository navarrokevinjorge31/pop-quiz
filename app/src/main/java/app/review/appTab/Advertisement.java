package app.review.appTab;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.jetbrains.annotations.Nullable;

import java.util.concurrent.TimeUnit;

import app.review.R;

/**
 * Created by cp on 1/31/18.
 */

public class Advertisement extends Activity {

    Button button_skip;
    String remainingTime , question , answer , finalAnswer , correct_desc
            , ques_ads_img_name , ques_ads_img_ext , ques_ads_vid_name , ques_ads_vid_ext , ques_ads_vid_thumbnail;
    TextView timer , txt_question , txt_answer , txt1;
    ImageView img_ads ,rightAnswer , wrongAnswer;
    VideoView vid_ads;
    CountDownTimer countDownTimer;
    double totalTimeCountInMilliseconds;
    long timeBlinkInMilliseconds,  result;
    boolean blink;

    LinearLayout upperLayout , middleLayout , lowerLayout , answerLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_advertisement);

        upperLayout = findViewById(R.id.upperLayout);
        middleLayout = findViewById(R.id.middleLayout);
        lowerLayout = findViewById(R.id.lowerLayout);
        answerLayout  = findViewById(R.id.answerLayout);

        upperLayout.setVisibility(View.VISIBLE);
        middleLayout.setVisibility(View.VISIBLE);
        lowerLayout.setVisibility(View.VISIBLE);
        answerLayout.setVisibility(View.GONE);

        Intent intent = getIntent();
        remainingTime = intent.getStringExtra("remainingTime");
        question = intent.getStringExtra("question");
        answer = intent.getStringExtra("answer");
        correct_desc = intent.getStringExtra("correct_desc");
        finalAnswer = intent.getStringExtra("finalAnswer");
        ques_ads_img_name = intent.getStringExtra("ques_ads_img_name");
        ques_ads_img_ext = intent.getStringExtra("ques_ads_img_ext");
        ques_ads_vid_name =  intent.getStringExtra("ques_ads_vid_name");
        ques_ads_vid_ext = intent.getStringExtra("ques_ads_vid_ext");
        ques_ads_vid_thumbnail = intent.getStringExtra("ques_ads_vid_thumbnail");

        Log.e("ads_info",ques_ads_img_name+"\n"+
                ques_ads_img_ext+"\n"+
                ques_ads_vid_name+"\n"+
                ques_ads_vid_ext+"\n"+
                ques_ads_vid_thumbnail);

        Log.e("answer",correct_desc + " " + finalAnswer);

        txt_question = findViewById(R.id.txt_question);
        txt_answer = findViewById(R.id.txt_answer);
        txt1 = findViewById(R.id.txt1);

        img_ads = findViewById(R.id.img_ads);
        rightAnswer = findViewById(R.id.rightAnswer);
        wrongAnswer  = findViewById(R.id.wrongAnswer);
        vid_ads = findViewById(R.id.vid_ads);

        //LOADING ADS IMAGE
        if(ques_ads_img_ext.equals("png")){

            img_ads.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load("https://crowdpoint.org/pq_uploads/images/"+ques_ads_img_name)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(img_ads);

        }else if(ques_ads_img_ext.equals("jpg")){

            img_ads.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load("https://crowdpoint.org/pq_uploads/images/"+ques_ads_img_name)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(img_ads);

        }else{
            img_ads.setVisibility(View.GONE);
        }

        //LOADING ADS VIDEO
        if(ques_ads_vid_ext.equals("mp4")){
            vid_ads.setVisibility(View.VISIBLE);
            try {
                // Start the MediaController

                MediaController mediacontroller = new MediaController(
                        Advertisement.this);
                mediacontroller.setAnchorView(vid_ads);
                // Get the URL from String VideoURL
                Log.d("videoUrl","https://crowdpoint.org/pq_uploads/videos/"+ques_ads_vid_ext);
                Uri video = Uri.parse("https://crowdpoint.org/pq_uploads/videos/"+ques_ads_vid_ext);
                vid_ads.setMediaController(mediacontroller);
                vid_ads.setVideoURI(video);
                vid_ads.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        vid_ads.start();
                    }
                });

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }else{
            vid_ads.setVisibility(View.GONE);
        }


        button_skip = findViewById(R.id.button_skip);
        button_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        timer = findViewById(R.id.timer);
        timer.setText(remainingTime);

        long min = Integer.parseInt(remainingTime.substring(0, 2));
        long sec = Integer.parseInt(remainingTime.substring(3));
        long t = (min * 60L) + sec;
        result = TimeUnit.SECONDS.toMillis(t);
        Log.e("remainingTime",String.valueOf(result));
        startTimer();

    }

//    public void setTimer() {
//        double time = 0;
//        double dateDifference = (int) getDateDiff(new SimpleDateFormat("HH:mm:ss"), timer.getText().toString() , "00:00:00");
//        System.out.println("dateDifference: "+ dateDifference);
//        time = dateDifference+1;
//        totalTimeCountInMilliseconds = 60 * time * 1000;
//        timeBlinkInMilliseconds = 30 * 1000;
//        startTimer();
//
//    }
//
//    public static double getDateDiff(SimpleDateFormat format, String rTime , String oldDate) {
//        try {
//            return TimeUnit.MINUTES.convert(format.parse(rTime).getTime() - format.parse(oldDate).getTime() , TimeUnit.MILLISECONDS);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return 0;
//        }
//    }

    public void startTimer() {

        countDownTimer = new CountDownTimer( result, 500) {
            // 500 means, onTick function will be called at every 500
            // milliseconds

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;

                if (leftTimeInMilliseconds < timeBlinkInMilliseconds) {
                    timer.setTextAppearance(getApplicationContext(),
                            R.style.blinkText);
                    // change the style of the textview .. giving a red
                    // alert style

                    if (blink) {
                        timer.setVisibility(View.VISIBLE);
                        // if blink is true, textview will be visible
                    } else {
                        timer.setVisibility(View.INVISIBLE);
                    }

                    blink = !blink; // toggle the value of blink
                }

                timer.setText(String.format("%02d", seconds / 60)
                        + ":" + String.format("%02d", seconds % 60));
                // format the textview to show the easily readable format

            }

            @Override
            public void onFinish() {

                qTimesUp();
            }
        }.start();
    }

    public void qTimesUp(){

        upperLayout.setVisibility(View.GONE);
        middleLayout.setVisibility(View.GONE);
        lowerLayout.setVisibility(View.VISIBLE);
        answerLayout.setVisibility(View.VISIBLE);
        button_skip.setText("CLOSE");

        if(finalAnswer.equals(correct_desc)){
            txt1.setText("");
            txt_answer.setText(answer+": "+correct_desc);
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rightAnswer.setBackgroundDrawable( getResources().getDrawable(R.drawable.right_answer) );
            } else {
                rightAnswer.setBackground( getResources().getDrawable(R.drawable.right_answer));
            }
        }else{
            txt1.setText("");
            txt_answer.setText(answer+": "+correct_desc);
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rightAnswer.setBackgroundDrawable( getResources().getDrawable(R.drawable.wrong_answer) );
            } else {
                rightAnswer.setBackground( getResources().getDrawable(R.drawable.wrong_answer));
            }
        }

        txt_question.setText(question);

    }

    @Override
    public void onBackPressed() {
    }
}
