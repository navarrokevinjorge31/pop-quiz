package app.review.appTab;

/**
 * Created by gics_admin on 2/1/2018.
 */

public class TabItem {

    public String center_season_id;
    public String rev_center_id;
    public String created_by;
    public String season_name;
    public String season_status;
    public String season_type;
    public String season_created;
    public String participant;
    public String season_updated;
    public String season_profile;
    public String paid_status;



    public TabItem(
            String center_season_id,
            String rev_center_id,
            String created_by,
            String season_name,
            String season_status,
            String season_type,
            String season_created,
            String season_updated,
            String participant,
            String season_profile,
            String paid_status){


        this.center_season_id = center_season_id;
        this.rev_center_id = rev_center_id;
        this.created_by = created_by;
        this.season_name = season_name;
        this.season_status = season_status;
        this.season_type = season_type;
        this.season_created = season_created;
        this.season_updated = season_updated;
        this.participant = participant;
        this.season_profile = season_profile;
        this.paid_status = paid_status;

    }
}
