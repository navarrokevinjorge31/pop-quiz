package app.review.appTab;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.review.AppController;
import app.review.Config;
import app.review.MessageDialog;
import app.review.R;
import app.review.appAuthentication.SessionManager;

/**
 * Created by gics_admin on 2/1/2018.
 */

public class TabActivity1 extends Activity {

    SessionManager session;
    String userid , token;
    MessageDialog m;
    private String TAG = DashBoard.class.getSimpleName();
    private String tag_string_req = "string_req";
    private List<TabItem> listDash;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private TabAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab1);

        session = new SessionManager(this);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> usr = session.getUserID();
        userid = usr.get(SessionManager.KEY_USERID);

        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));

        listDash = new ArrayList<>();
        mAdapter = new TabAdapter(listDash, TabActivity1.this , new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d(TAG, "clicked position:" + i);
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                //newmakeStringReq();
            }
        });


        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayoutFragment);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                listDash.clear();
                makeStringReq();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        makeStringReq();
    }

    private void makeStringReq() {
        //Config.RANKING+"/2/"+String.valueOf(offset)+"/"+token
//        loading.setVisibility(View.VISIBLE);
        final StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.ACTIVITIES+"/"+userid+"/"+token, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString() );
                try {

                    JSONObject obj = new JSONObject(response);
                    String success = obj.getString("success");
                    String message = obj.getString("message");
                    JSONObject c = obj.getJSONObject("data");
                    JSONArray d = c.getJSONArray("seasons");

                    for (int i = 0; i < d.length(); i++) {
                        JSONObject feedObj = d.getJSONObject(i);

                        String center_season_id = feedObj.getString("center_season_id");
                        String rev_center_id = feedObj.getString("rev_center_id");
                        String created_by = feedObj.getString("created_by");
                        String season_name = feedObj.getString("season_name");
                        String season_status = feedObj.getString("season_status");
                        String season_type = feedObj.getString("season_type");
                        String season_created = feedObj.getString("season_created");
                        String season_updated = feedObj.getString("season_updated");
                        String participant = feedObj.getString("participant");
                        String season_profile = feedObj.getString("season_profile");
                        String paid_status = feedObj.getString("paid_status");

                        TabItem m = new TabItem(
                                center_season_id,
                                rev_center_id,
                                created_by,
                                season_name,
                                season_status,
                                season_type,
                                season_created,
                                season_updated,
                                participant,
                                season_profile,
                                paid_status);

                        listDash.add(m);
                    }

                    mAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }

        });
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        Log.d("strReq", String.valueOf(strReq));

    }
}
