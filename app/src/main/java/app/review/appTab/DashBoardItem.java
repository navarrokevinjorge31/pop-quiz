package app.review.appTab;

/**
 * Created by cp on 11/28/17.
 */

public class DashBoardItem {

    public String user_rank;
    public String user_fullname;
    public String user_email;
    public String user_profile_picture;
    public String user_provider;
    public String user_level;
    public String user_rev_center_id;
    public String user_school;
    public String user_school_logo;
    public String answer_submited;
    public String correct_answer;
    public String incorrect_answer;
    public String positive_score;
    public String negative_score;
    public String timeup_correct_answer;
    public String timeup_incorrect_answer;
    public String timeup_positive_score;
    public String timeup_negative_score;


    public DashBoardItem(
                        String user_rank,
                        String user_fullname,
                        String user_email,
                        String user_profile_picture,
                        String user_provider,
                        String user_level,
                        String user_rev_center_id,
                        String user_school,
                        String user_school_logo,
                        String answer_submited,
                        String correct_answer,
                        String incorrect_answer,
                        String positive_score,
                        String negative_score,
                        String timeup_correct_answer,
                        String timeup_incorrect_answer,
                        String timeup_positive_score,
                        String timeup_negative_score){


        this.user_rank = user_rank;
        this.user_fullname = user_fullname;
        this.user_email = user_email;
        this.user_profile_picture = user_profile_picture;
        this.user_provider = user_provider;
        this.user_level = user_level;
        this.user_rev_center_id = user_rev_center_id;
        this.user_school = user_school;
        this.user_school_logo = user_school_logo;
        this.answer_submited = answer_submited;
        this.correct_answer = correct_answer;
        this.incorrect_answer = incorrect_answer;
        this.positive_score = positive_score;
        this.negative_score = negative_score;
        this.timeup_correct_answer = timeup_correct_answer;
        this.timeup_incorrect_answer = timeup_incorrect_answer;
        this.timeup_positive_score = timeup_positive_score;
        this.timeup_negative_score = timeup_negative_score;

    }
}
