package app.review.appTab;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.support.v7.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.review.AppController;
import app.review.Config;
import app.review.MessageDialog;
import app.review.R;
import app.review.appAuthentication.SessionManager;

/**
 * Created by cp on 11/23/17.
 */

public class DashBoard extends AppCompatActivity implements DashBoardAdapter.DashboardAdapterListener {

    private String TAG = DashBoard.class.getSimpleName();
    private String tag_string_req = "string_req";
    String  center_season_id;
    private List<DashBoardItem> listDash;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private DashBoardAdapter mAdapter;

    String token;

    int offset , offset1;

    SessionManager session;

    FrameLayout loading;

    RelativeLayout nopost;

    Dialog alert;

    SwipeRefreshLayout swipeRefreshLayout;

    MessageDialog m;

    private SearchView searchView;

    TextView progressstatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_dashboard);

        Intent intent = getIntent();
        center_season_id = intent.getStringExtra("center_season_id");
        session = new SessionManager(this);
        HashMap<String, String> tkn = session.getToken();
        token = tkn.get(SessionManager.KEY_TOKEN);

//        m = new MessageDialog();
//        m.makeDialog(DashBoard.this,"Ranking is not yet Published");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Scoreboard");

        progressstatus = findViewById(R.id.progressstatus);
        loading = findViewById(R.id.loading);
        nopost = findViewById(R.id.nopost);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));

        listDash = new ArrayList<>();
        mAdapter = new DashBoardAdapter(listDash, DashBoard.this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                //newmakeStringReq();
            }
        });

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayoutFragment);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                listDash.clear();
                offset = 0;
                makeStringReq();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        makeStringReq();

    }

    private void makeStringReq() {
    //Config.RANKING+"/2/"+String.valueOf(offset)+"/"+token
        loading.setVisibility(View.VISIBLE);
        final StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.RANKING+"/"+center_season_id+"/null/"+token, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString() );
                try {
                    loading.setVisibility(View.GONE);
                    JSONObject obj = new JSONObject(response);
                    String success = obj.getString("success");
                    String message = obj.getString("message");
                    JSONObject c = obj.getJSONObject("data");
                    offset = (Integer.parseInt(c.getString("offset")) + 10);
                    offset1 = (Integer.parseInt(c.getString("offset")));
                    JSONArray d = c.getJSONArray("allscore");

                    for (int i = 0; i < d.length(); i++) {
                        JSONObject feedObj = d.getJSONObject(i);

                        String user_rank = String.valueOf(i);
                        String user_fullname = feedObj.getString("user_fullname");
                        String user_email = feedObj.getString("user_email");
                        String user_profile_picture = feedObj.getString("user_profile_picture");
                        String user_provider = feedObj.getString("user_provider");
                        String user_level = feedObj.getString("user_level");
                        String user_rev_center_id = feedObj.getString("user_rev_center_id");
                        String user_school = feedObj.getString("user_school");
                        String user_school_logo = feedObj.getString("user_school_logo");
                        String answer_submited = feedObj.getString("answer_submited");
                        String correct_answer = feedObj.getString("correct_answer");
                        String incorrect_answer = feedObj.getString("incorrect_answer");
                        String positive_score = feedObj.getString("positive_score");
                        String negative_score = feedObj.getString("negative_score");
                        String timeup_correct_answer = feedObj.getString("timeup_correct_answer");
                        String timeup_incorrect_answer = feedObj.getString("timeup_incorrect_answer");
                        String timeup_positive_score = feedObj.getString("timeup_positive_score");
                        String timeup_negative_score = feedObj.getString("timeup_negative_score");


                        DashBoardItem m = new DashBoardItem(
                                user_rank,
                                user_fullname,
                                user_email,
                                user_profile_picture,
                                user_provider,
                                user_level,
                                user_rev_center_id,
                                user_school,
                                user_school_logo,
                                answer_submited,
                                correct_answer,
                                incorrect_answer,
                                positive_score,
                                negative_score,
                                timeup_correct_answer,
                                timeup_incorrect_answer,
                                timeup_positive_score,
                                timeup_negative_score);

                        listDash.add(m);

                    }
                    progressstatus.setText("Participants Name ("+String.valueOf(d.length()+")"));
                    mAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }

        });
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        Log.d("strReq", String.valueOf(strReq));

    }

    public void newmakeStringReq(){

        final StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.RANKING+"/2/"+String.valueOf(offset)+"/"+token, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString() );
                try {
                    loading.setVisibility(View.GONE);
                    JSONObject obj = new JSONObject(response);
                    String success = obj.getString("success");
                    String message = obj.getString("message");
                    JSONObject c = obj.getJSONObject("data");
                    offset = (Integer.parseInt(c.getString("offset")) + 10);
                    offset1 = (Integer.parseInt(c.getString("offset")));
                    JSONArray d = c.getJSONArray("allscore");

                    for (int i = 0; i < d.length(); i++) {
                        JSONObject feedObj = d.getJSONObject(i);

                        String user_rank = String.valueOf(i++);
                        String user_fullname = feedObj.getString("user_fullname");
                        String user_email = feedObj.getString("user_email");
                        String user_profile_picture = feedObj.getString("user_profile_picture");
                        String user_provider = feedObj.getString("user_provider");
                        String user_level = feedObj.getString("user_level");
                        String user_rev_center_id = feedObj.getString("user_rev_center_id");
                        String user_school = feedObj.getString("user_school");
                        String user_school_logo = feedObj.getString("user_school_logo");
                        String answer_submited = feedObj.getString("answer_submited");
                        String correct_answer = feedObj.getString("correct_answer");
                        String incorrect_answer = feedObj.getString("incorrect_answer");
                        String positive_score = feedObj.getString("positive_score");
                        String negative_score = feedObj.getString("negative_score");
                        String timeup_correct_answer = feedObj.getString("timeup_correct_answer");
                        String timeup_incorrect_answer = feedObj.getString("timeup_incorrect_answer");
                        String timeup_positive_score = feedObj.getString("timeup_positive_score");
                        String timeup_negative_score = feedObj.getString("timeup_negative_score");


                        DashBoardItem m = new DashBoardItem(
                                user_rank,
                                user_fullname,
                                user_email,
                                user_profile_picture,
                                user_provider,
                                user_level,
                                user_rev_center_id,
                                user_school,
                                user_school_logo,
                                answer_submited,
                                correct_answer,
                                incorrect_answer,
                                positive_score,
                                negative_score,
                                timeup_correct_answer,
                                timeup_incorrect_answer,
                                timeup_positive_score,
                                timeup_negative_score);
                        listDash.add(m);

                    }
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }

        });
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        Log.d("strReq", String.valueOf(strReq));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                Log.d("search",query);
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.d("search",query);
                // filter recycler view when text is changed
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContactSelected(DashBoardItem contact) {

    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }
}
