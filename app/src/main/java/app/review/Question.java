package app.review;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.review.appAuthentication.SessionManager;
import app.review.appTab.Advertisement;

/**
 * Created by cp on 11/24/17.
 */

public class Question extends Activity implements View.OnClickListener {

    private String TAG = Question.class.getSimpleName();
    SessionManager session;
    String correctanswer,userid , token , pub_id , ques_id, letter, finalAnswer , finalAnswer1 , pub_time_limit , pub_created ,
           q_time , q_date , pub_date_limit , standard_time , ques_correct , pub_ques_points , quest_type , difficulties ,correct_desc,
            ques_ads_img_name ,ques_ads_img_ext , ques_ads_vid_name , ques_ads_vid_ext,ques_ads_vid_thumbnail;
    String ques_opt_a;
    String ques_opt_b;
    String ques_opt_c;
    String ques_opt_d;
    TextView question , optA , optB , optC , optD , timer , questionTopic , totalPoints , quesType;
    ImageView question_pic , optA_pic , optB_pic , optC_pic , optD_pic;
    FrameLayout loading;
    long diff;
    CountDownTimer countDownTimer;
    long totalTimeCountInMilliseconds;
    long timeBlinkInMilliseconds;
    boolean blink;

    public Dialog confirmAnswer , timesUp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_question);

        Intent intent = getIntent();
        pub_id = intent.getStringExtra("pub_id");

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        q_time = tf.format(c.getTime());
        q_date = df.format(c.getTime());
        // formattedDate have current date/time
        //Toast.makeText(this, q_date, Toast.LENGTH_SHORT).show();

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> uid = session.getUserID();
        HashMap<String, String> tkn = session.getToken();
        userid = uid.get(SessionManager.KEY_USERID);
        token = tkn.get(SessionManager.KEY_TOKEN);
        Log.d("details", userid + "\n" + token + "\n" + pub_id);

        timer = findViewById(R.id.timer);
        loading = findViewById(R.id.loading);
        question = findViewById(R.id.question);
        questionTopic = findViewById(R.id.questionTopic);
        totalPoints = findViewById(R.id.totalPoints);
        quesType = findViewById(R.id.quesType);
        optA = findViewById(R.id.optA);
        optB = findViewById(R.id.optB);
        optC = findViewById(R.id.optC);
        optD = findViewById(R.id.optD);

        question_pic = findViewById(R.id.question_pic);
        optA_pic = findViewById(R.id.optA_pic);
        optB_pic = findViewById(R.id.optB_pic);
        optC_pic = findViewById(R.id.optC_pic);
        optD_pic = findViewById(R.id.optD_pic);

        optA.setOnClickListener(this);
        optB.setOnClickListener(this);
        optC.setOnClickListener(this);
        optD.setOnClickListener(this);
        question_pic.setOnClickListener(this);
        optA_pic.setOnClickListener(this);
        optB_pic.setOnClickListener(this);
        optC_pic.setOnClickListener(this);
        optD_pic.setOnClickListener(this);

        getQuestion();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.optA) {
            finalAnswer = optA.getText().toString();
            finalAnswer1 = ques_opt_a;
            letter = "a";
            confirmAnswer();
        } else if (view.getId() == R.id.optB) {
            finalAnswer = optB.getText().toString();
            finalAnswer1 = ques_opt_b;
            letter = "b";
            confirmAnswer();
        } else if (view.getId() == R.id.optC) {
            finalAnswer = optC.getText().toString();
            finalAnswer1 = ques_opt_c;
            letter = "c";
            confirmAnswer();
        } else if (view.getId() == R.id.optD) {
            finalAnswer = optD.getText().toString();
            finalAnswer1 = ques_opt_d;
            letter = "d";
            confirmAnswer();
        } else if (view.getId() == R.id.optA_pic) {
            finalAnswer = optA.getText().toString();
            letter = "a";
            confirmAnswer();
        } else if (view.getId() == R.id.optB_pic) {
            finalAnswer = optB.getText().toString();
            letter = "b";
            confirmAnswer();
        } else if (view.getId() == R.id.optC_pic) {
            finalAnswer = optC.getText().toString();
            letter = "c";
            confirmAnswer();
        } else if (view.getId() == R.id.optD_pic) {
            finalAnswer = optD.getText().toString();
            letter = "d";
            confirmAnswer();
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void getQuestion(){

        loading.setVisibility(View.VISIBLE);
        String url = Config.QUESTION +"/"+pub_id+"/"+userid+"/"+token;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");

                            JSONObject c = obj.getJSONObject("data");
                            JSONObject d = c.getJSONObject("question");

                            String pub_id = d.getString("pub_id");
                            correct_desc = d.getString("correct_desc");
                            correctanswer = d.getString("ques_correct");
                            ques_correct = d.getString("ques_correct");
                            ques_id = d.getString("ques_id");
                            pub_time_limit = d.getString("pub_time_limit");
                            pub_created = d.getString("pub_created");
                            pub_date_limit = d.getString("pub_date_limit");
                            standard_time = d.getString("standard_time");
                            pub_ques_points = d.getString("pub_ques_points");
                            quest_type = d.getString("quest_type");
                            difficulties = d.getString("difficulties");
                            ques_ads_img_name = d.getString("ques_ads_img_name");
                            ques_ads_img_ext = d.getString("ques_ads_img_ext");
                            ques_ads_vid_name = d.getString("ques_ads_vid_name");
                            ques_ads_vid_ext = d.getString("ques_ads_vid_ext");
                            ques_ads_vid_thumbnail = d.getString("ques_ads_vid_thumbnail");
                            String categ_desc = d.getString("categ_desc");
                            String ques_desc = d.getString("ques_desc");
                            ques_opt_a = d.getString("ques_opt_a");
                            ques_opt_b = d.getString("ques_opt_b");
                            ques_opt_c = d.getString("ques_opt_c");
                            ques_opt_d = d.getString("ques_opt_d");

                            String qques_desc_pic = d.getString("ques_desc_pic");
                            String qques_opt_a_pic = d.getString("ques_opt_a_pic");
                            String qques_opt_b_pic = d.getString("ques_opt_b_pic");
                            String qques_opt_c_pic = d.getString("ques_opt_c_pic");
                            String qques_opt_d_pic = d.getString("ques_opt_d_pic");

                            questionTopic.setText("Topic: " + categ_desc);
                            quesType.setText("Question Type: " + quest_type);
                            totalPoints.setText(difficulties + " - " + pub_ques_points + " points");
                            question.setText(ques_desc);
                            optA.setText("A. " + ques_opt_a);
                            optB.setText("B. " + ques_opt_b);
                            optC.setText("C. " + ques_opt_c);
                            optD.setText("D. " + ques_opt_d);

                            Glide.with(Question.this)
                                    .load("https://crowdpoint.org/pq_uploads/images/"+qques_desc_pic)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(question_pic);

                            Glide.with(Question.this)
                                    .load("https://crowdpoint.org/pq_uploads/images/"+qques_opt_a_pic)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(optA_pic);

                            Glide.with(Question.this)
                                    .load("https://crowdpoint.org/pq_uploads/images/"+qques_opt_b_pic)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(optB_pic);

                            Glide.with(Question.this)
                                    .load("https://crowdpoint.org/pq_uploads/images/"+qques_opt_c_pic)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(optC_pic);

                            Glide.with(Question.this)
                                    .load("https://crowdpoint.org/pq_uploads/images/"+qques_opt_d_pic)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(optD_pic);

                            loading.setVisibility(View.GONE);
//                            timer.setTextAppearance(getApplicationContext(),
//                                    R.style.normalText);
                            setTimer();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pub_id", pub_id);
                params.put("user_unique_id", userid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        Log.d("strReq", String.valueOf(url));
        queue.add(postRequest);
    }

    public void setTimer() {

        if(!pub_date_limit.equals(q_date)){

            qExpired();

        }else{

            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            try {

                Date oldDate = dateFormat.parse(standard_time);
                Date currentDate = dateFormat.parse(pub_time_limit);

                diff = currentDate.getTime() - oldDate.getTime();
                long seconds = diff / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;
                Log.e("diff", "minutes:"+diff);

                if (oldDate.before(currentDate)) {
                    Log.e("standard_time", "is previous date");
                    Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                            + " hours: " + hours + " days: " + days);

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int time = 0;
            int dateDifference = (int) getDateDiff(new SimpleDateFormat("HH:mm:ss"), standard_time, pub_time_limit);
            System.out.println("dateDifference: " + pub_time_limit +  " - " + standard_time + " = "+ dateDifference);
            time = dateDifference + 1;
            totalTimeCountInMilliseconds = 60 * time * 1000;
            timeBlinkInMilliseconds = 30 * 1000;
            startTimer();

        }

    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.MINUTES.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime() , TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void startTimer() {
        countDownTimer = new CountDownTimer(diff, 500) {
            // 500 means, onTick function will be called at every 500
            // milliseconds

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;

                if (leftTimeInMilliseconds < timeBlinkInMilliseconds) {
                    timer.setTextAppearance(getApplicationContext(),
                            R.style.blinkText);
                    // change the style of the textview .. giving a red
                    // alert style

                    if (blink) {
                        timer.setVisibility(View.VISIBLE);
                        // if blink is true, textview will be visible
                    } else {
                        timer.setVisibility(View.INVISIBLE);
                    }

                    blink = !blink; // toggle the value of blink
                }

                timer.setText(String.format("%02d", seconds / 60)
                        + ":" + String.format("%02d", seconds % 60));
                // format the textview to show the easily readable format

            }

            @Override
            public void onFinish() {

                qTimesUp();
            }
        }.start();
    }

    public void qTimesUp(){

        timesUp = new Dialog(Question.this);
        timesUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        timesUp.setCancelable(false);
        timesUp.setContentView(R.layout.xml_times_up1);
        Window window = timesUp.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button b_ok = timesUp.findViewById(R.id.button_ok);
        b_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (!Question.this.isFinishing()){
            timesUp.show();
        }
        //timesUp.show();
    }

    public void qExpired(){

        timesUp = new Dialog(Question.this);
        timesUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        timesUp.setCancelable(false);
        timesUp.setContentView(R.layout.xml_times_up);
        Window window = timesUp.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView t_message = timesUp.findViewById(R.id.txt_message);
        t_message.setText("SORRY! \n THIS QUESTION IS EXPIRED \n TRY AGAIN NEXT POP QUIZ \n");
        Button b_ok = timesUp.findViewById(R.id.button_ok);
        b_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (!Question.this.isFinishing()){
            timesUp.show();
        }
    }

    public void confirmAnswer(){

        confirmAnswer = new Dialog(this);
        confirmAnswer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmAnswer.setContentView(R.layout.xml_confirm_answer);
        Window window = confirmAnswer.getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        final FrameLayout loading = confirmAnswer.findViewById(R.id.loading);
        final TextView answer = confirmAnswer.findViewById(R.id.answer);
        Button submit = confirmAnswer.findViewById(R.id.submit);
        Button change = confirmAnswer.findViewById(R.id.change);

        answer.setText("Are you sure your answer is \n" + finalAnswer);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading.setVisibility(View.VISIBLE);

                String url = Config.ANSWER;
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("Response", response);
                                try {
                                    JSONObject obj = new JSONObject(response);
                                    String success = obj.getString("success");
                                    if(success.equals("false")){
                                        Toast.makeText(Question.this, "You already answered this question", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }else{
                                        Toast.makeText(Question.this, "Answer Submitted", Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(getApplicationContext(), Advertisement.class);
                                        i.putExtra("answer",correctanswer);
                                        i.putExtra("correct_desc",correct_desc);
                                        i.putExtra("finalAnswer",finalAnswer1);
                                        i.putExtra("remainingTime",timer.getText().toString());
                                        i.putExtra("question",question.getText().toString());
                                        i.putExtra("ques_ads_img_name",ques_ads_img_name);
                                        i.putExtra("ques_ads_img_ext",ques_ads_img_ext);
                                        i.putExtra("ques_ads_vid_name",ques_ads_vid_name);
                                        i.putExtra("ques_ads_vid_ext",ques_ads_vid_ext);
                                        i.putExtra("ques_ads_vid_thumbnail",ques_ads_vid_thumbnail);
                                        startActivity(i);
                                        finish();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                VolleyLog.d(TAG, "Error: " + error.getMessage());

                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("pub_id", pub_id);
                        params.put("user_answer", letter);
                        params.put("user_unique_id", userid);
                        params.put("token", token);
                        params.put("ques_id", ques_id);

                        return params;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                queue.add(postRequest);


            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAnswer.hide();
            }
        });

        if (!Question.this.isFinishing()){
            confirmAnswer.show();
        }
        //confirmAnswer.show();
    }


}