package app.review.appUserManagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import app.review.MessageDialog;
import app.review.R;
import app.review.appAuthentication.SessionManager;

/**
 * Created by cp on 12/13/17.
 */

public class RegisterA extends AppCompatActivity {

    RelativeLayout next;
    EditText et_fName , et_lName , et_email , et_address , et_cnumber;
    SessionManager session;
    MessageDialog m;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_register_a);

        m = new MessageDialog();
        session = new SessionManager(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("I have existing Account");

        et_fName = findViewById(R.id.et_fName);
        et_lName = findViewById(R.id.et_lName);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        et_cnumber = findViewById(R.id.et_cnumber);

        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_fName.getText().toString().equals("")||et_fName.getText().toString().equals(null)){

                    m.makeDialog(RegisterA.this,"First name is required! \n Please fill up.");

                }else if(et_lName.getText().toString().equals("")||et_lName.getText().toString().equals(null)){

                    m.makeDialog(RegisterA.this,"Last name is required! \n Please fill up.");

                }else if(et_email.getText().toString().equals("")||et_email.getText().toString().equals(null)){

                    m.makeDialog(RegisterA.this,"Email address is required! \n Please fill up.");

                }else{

                    session.setFname(et_fName.getText().toString());
                    session.setLname(et_lName.getText().toString());
                    session.setEmail(et_email.getText().toString());
                    session.setAddress(et_address.getText().toString());
                    session.setCnumber(et_address.getText().toString());
                    Intent i = new Intent(RegisterA.this,RegisterB.class);
                    startActivity(i);
                }

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
