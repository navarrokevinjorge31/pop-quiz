package app.review.appUserManagement;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import app.review.Config;
import app.review.MessageDialog;
import app.review.R;
import app.review.appAuthentication.SessionManager;

/**
 * Created by cp on 12/13/17.
 */

public class RegisterC extends AppCompatActivity {

    public static final String TAG = RegisterC.class.getSimpleName();
    SessionManager session;
    String fName , lName , email , address , cnumber , birthday , gender , deviceid , userid , token ,user;
    RelativeLayout next;
    Thread t;
    InstanceID instanceID;
    Context applicationContext;
    FrameLayout loading;
    TextView txt_loading_text;
    EditText et_password , et_cPassword;
    MessageDialog m;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_register_c);

        t = new Thread() {

            @Override
            public void run() {
                try {


                    while (!isInterrupted()) {
                        Thread.sleep(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(deviceid==null){
                                    registerInBackground();
                                }else{
                                    t.isInterrupted();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        m = new MessageDialog();
        session = new SessionManager(getApplicationContext());
        instanceID = InstanceID.getInstance(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Back to Birthday and Gender");

        loading = findViewById(R.id.loading);
        txt_loading_text = findViewById(R.id.loadingText);
        next = findViewById(R.id.next);
        et_password = findViewById(R.id.et_password);
        et_cPassword = findViewById(R.id.et_cPassword);

        HashMap<String, String> firstName = session.getFname();
        HashMap<String, String> lastName = session.getLname();
        HashMap<String, String> eadd = session.getEmail();
        HashMap<String, String> bday = session.getBirthday();
        HashMap<String, String> sex = session.getGender();
        HashMap<String, String> addrss = session.getAddress();
        HashMap<String, String> cnum = session.getCnumber();
        fName = firstName.get(SessionManager.FNAME);
        lName = lastName.get(SessionManager.LNAME);
        email = eadd.get(SessionManager.EMAIL);
        birthday = bday.get(SessionManager.BIRTHDAY);
        gender = sex.get(SessionManager.GENDER);
        address = addrss.get(SessionManager.ADDRESS);
        cnumber = cnum.get(SessionManager.CONTACT_NUMBER);
        Log.d("REGISTER DATA", fName +"\n"+
                lName +"\n"+
                email +"\n"+
                address +"\n"+
                birthday +"\n"+
                cnumber +"\n"+
                gender );


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_password.getText().toString().equals("")||et_password.getText().toString().equals(null)){

                    m.makeDialog(RegisterC.this,"Password is required! \n Please fill up.");

                }else if(et_cPassword.getText().toString().equals("")||et_cPassword.getText().toString().equals(null)){

                    m.makeDialog(RegisterC.this,"Confirm Password is required! \n Please fill up.");

                }else if(!et_cPassword.getText().toString().equals(et_cPassword.getText().toString())){

                    m.makeDialog(RegisterC.this,"Password did not match! \n Please check.");

                }
                registerNew();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void registerNew(){

        loading.setVisibility(View.VISIBLE);

        String url = Config.REGISTER;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            if(success.equals("false")){
                                loading.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "invalid account", Toast.LENGTH_SHORT).show();
                            }
                            JSONObject c = obj.getJSONObject("data");
                            String userId = c.getString("user_unique_id");
                            String token = c.getString("token");
                            String userLevel = c.getString("user_level");
                            String user_prof_id = c.getString("user_prof_id");
                            String isEmailConfirmed = c.getString("isEmailConfirmed");

                            session.setUserID(userId);
                            session.setToken(token);
                            session.setPermission(userLevel);
                            session.setProfileId(user_prof_id);
                            session.setEmailConrfirm(isEmailConfirmed);
                            Log.d("session",userId +"\n"+ token +"\n"+ userLevel);

                            getDeviceID();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                        loading.setVisibility(View.GONE);
                    }

                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_fname", fName);
                params.put("user_mname", "null");
                params.put("user_lname", lName);
                params.put("fullname", fName + " " + lName);
                params.put("birthdate", birthday);
                params.put("gender", gender);
                params.put("mobile_id", deviceid);
                params.put("profile_picture", "none");
                params.put("user_contact", cnumber);
                params.put("user_address", address);
                params.put("email", email);
                params.put("password", et_cPassword.getText().toString());
                params.put("center_season_id", "2");
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
        Log.d("strReq", String.valueOf(url));



    }

    public void getDeviceID(){
        txt_loading_text.setText("Logging in...");
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> uid = session.getUserID();
        HashMap<String, String> tkn = session.getToken();
        userid = uid.get(SessionManager.KEY_USERID);
        token = tkn.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> usr = session.getPermission();
        user = usr.get(SessionManager.KEY_PERMISSION);

        StringRequest postRequest = new StringRequest(Request.Method.POST, Config.DEVICEID,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(), " Login Successful!", Toast.LENGTH_SHORT).show();
                        finishAffinity();

                        t.interrupt();
                        Intent intent = new Intent(getApplicationContext(), PickImage.class);
                        startActivity(intent);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        VolleyLog.d(TAG, "Error: " + error.getMessage());

                        loading.setVisibility(View.GONE);


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_unique_id", userid);
                params.put("device_unique_id", deviceid);
                params.put("token", token);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(postRequest);

    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {

            }
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";

                try {
                    if (instanceID == null) {
                        instanceID = InstanceID.getInstance(applicationContext);
                    }
                    deviceid = instanceID.getToken(Config.GOOGLE_PROJ_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    msg = "Registration ID :" + deviceid;
                    Log.d("deviceid",deviceid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }

                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }
}
