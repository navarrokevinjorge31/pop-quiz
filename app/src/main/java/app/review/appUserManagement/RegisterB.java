package app.review.appUserManagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import app.review.MessageDialog;
import app.review.R;
import app.review.appAuthentication.SessionManager;

/**
 * Created by cp on 12/13/17.
 */

public class RegisterB extends AppCompatActivity {

    RelativeLayout next;
    DatePicker datePicker;
    RadioGroup radioSex;
    int selectedValueId;
    RadioButton male;
    String gender;
    SessionManager session;
    MessageDialog m;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_register_b);

        session = new SessionManager(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Back to Name Details");

        radioSex = findViewById(R.id.radioSex);
        datePicker = findViewById(R.id.datePicker);
        male = findViewById(R.id.radioMale);

        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //getting the id of selected radio button
                selectedValueId = radioSex.getCheckedRadioButtonId();
                //checking the id of the selected radio
                if (selectedValueId == male.getId()) {

                    gender = "male";
                } else {
                    gender = "female";
                }

                session.setBirthday(datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear());
                session.setGender(gender);
                Intent i = new Intent(RegisterB.this,RegisterC.class);
                startActivity(i);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
